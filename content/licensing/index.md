---
title: Licensing
menu: website
---
One of the last tasks is to compare open source licenses and select one for this website and possibly for the final project.

{{< blockquote author="open source initiative">}} Open source licenses are licenses that comply with the Open Source Definition – in brief, they allow software to be freely used, modified, and shared.{{</ blockquote>}}

None of the following licenses act as actual lisence statements for this website. For the actual license, see [CC-BY 4.0 International](https://creativecommons.org/licenses/by/4.0)

### The Unlicense

The Unlicense is a software license that completely absolves the licensor of any copyright and dedicates the creation into public domain. The license is short enough to post here in its entirety

"This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this software dedicate any and all copyright interest in the software to the public domain. We make this dedication for the benefit of the public at large and to the detriment of our heirs and successors. We intend this dedication to be an overt act of relinquishment in perpetuity of all present and future rights to this software under copyright law.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE."

The unlicense will not work with any of my projects on this website as I want to remain the copyright holder of all of my inventions even if the projects are openly sourced to others.

### The Fab license

Fab Academy provides its own license that is very similar to the unlicense in that it is very short and allows for almost anything BUT the fab license retains the licensor as the copyright holder.

"(c) holder date

This work may be reproduced, modified, distributed, performed, and displayed for any purpose, but must acknowledge "project name". Copyright is retained and must be preserved. The work is provided as is; no warranty is provided, and users accept all liability."

This license is better than the unlicense for the purpose I already mentioned. The fab license allows me to keep copyright but allows others to continue my work. The license seems, however, a little short and perhaps too concise. Some more robust legal language is preferred.

### Creative Commons

[Creative Commons](https://creativecommons.org) is an international nonprofit organization that provides multiple diffrent licenses for people to license their creative works.

The most permissible one is usually CC-BY which states that the work is free to use in any way, even commercially, as long as the user credits the author of the original work.

CC-BY-NC works in the same way but prohibits commercial use of the work (hence, non commercial, NC)

Other additions such as -ND (as in No Derivatives) allows only for distributing but not modifying the original work or -SA, which requires derivatives to be licensed under the same license.

Creative Commons also provides the CC0 public domain dedication tool that can be used similarly to The Unlicense to release copyright of works to the public domain.

For this documentation website and my final project I have chosen CC-BY 4.0 International as my license. It lets me keep the copyright for all of my projects but allows others to reuse any of my designs as long as I am attributed. For now as my livelyhood is not dependent on intellectual property I am happy to grant others the ability to benefit from my work. In the lucky moment such happens I can cash in the attribution in my CV.
