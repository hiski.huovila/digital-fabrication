---
title: Computer controlled machining
menu: project
weight: 6
---

CNC! The scary but efficient industry tool to cut anything you want (within reason). We were given 120x120cm 15mm plywood to make stuff and so I will.

### The design

What to make, what to make. Initially I wanted to make a dungeon master screen for tabletop roleplaying as my final project but I've come to realize the need for complex electronics in such a product is fairly small. So instead I'll work extra hard and make one this week instead.

{{< image src="sketches.jpg" alt="Black ink sketches of the design">}} This feels familiar {{</ image>}}

Then I went to CAD. I've finally gotten comfortable with FreeCAD and could design this fairly easily.

{{< image src="firstdesign.jpg" alt="3d model in FreeCAD">}} Two layers will be too thick {{</ image>}}

>I discarded that design due to me realizing the object would be a whopping 3cm thick if I used the plywood twice so that I could get a groove for the paper that I want to put inside so I designed this instead:

{{< image src="seconddesign.jpg" alt="2nd 3d model in FreeCAD">}} Now we are talking {{</ image>}}

I found some 5mm wide and 3mm thick neodymium magnets from the lab so I could use them in my design.

{{< image src="seconddesignannotated.jpg" alt="2nd 3d model in FreeCAD with annotations">}} A hinge in 1. and the magnets in 2. and 3. {{</ image>}}

I designed this test piece to test the reliability of the machine and see how good of a finish I can get on the pocket and any sides.

{{< image src="testdesign.jpg" alt="2nd 3d model in FreeCAD with annoations">}} Vcarve is an easy to use piece of software {{</ image>}}

Then I put in these settings

{{< image src="toolsettings.jpg" alt="Picture of  the settings menu used for the machining">}} A little faster, a little stronger {{</ image>}}

I also did an extra amount of small layer cuts near the top to reduce tearing

{{< image src="smoothing.jpg" alt="Picture of the cutting depth settings">}} Small cutting depth carves out less wood {{</ image>}}

And the carve will look like this

{{< image src="testpathing.jpg" alt="The cutting path in software">}} This is how we roll {{</ image>}}

I put the wood on the CNC, put the tool in and turned on the vacuum pump.

{{< image src="cncready.jpg" alt="Plywood affixed to the CNC">}} Allright {{</ image>}}

{{< image src="herewego.jpg" alt="Hand presses button">}} Here we goooo {{</ image>}}

### Disaster strikes

However, due to an error in the CNC machine the CNC cut too deep and started to carve on the machine bed.

{{< image src="ohno.jpg" alt="Cnc cut has come loose">}} That does not look right {{</ image>}}

The part came loose and I emergency stopped the machine. Notably I had an instructor with me to make sure my values were put in correctly so any errors should not be coming from me.

{{< image src="oops.jpg" alt="Damaged CNC cutting bed">}} Fortunately the CNC bed is still functional {{</ image>}}

#### Back on track

After testing the machine worked fine so I cut out these pieces

{{< image src="goodcut.jpg" alt="Test pieces cut from plywood">}} Nice cuts, no issues {{</ image>}}

{{< image src="testunassembled.jpg" alt="Test pieces cut from plywood">}} Almost like IKEA {{</ image>}}

{{< image src="testassembled.jpg" alt="Test pieces cut from plywood assembled together">}} Not a snap fit but pretty good {{</ image>}}

### Disaster strikes back

Content with my design I set out to cut the actual project. I put in this path:

{{< image src="dmscreenpath.jpg" alt="Toolpath for the final design">}} A bit bigger, a bit better {{</ image>}}

The idea was to get a nice surface finish by using downcut tools to break in the wood and then change to an upcutting tool.

{{< image src="dmscreencutting.jpg" alt="CNC cutting plywood">}} Looking good so far.{{</ image>}}

After changing to the upcutting tool the machine started acting up again and cut straight into the bed causing damage to the metal and the machine! I stopped the machine again. With the instructor we identified that the issue is caused by the machine forgetting its zeroing during cutting. Notably this is not something I could affect myself, so the machine might need reparing.

Next week we managed to figure out the reason for this issue. Turns out I had possibly fastened the tool too loosely and the upcutting pulled the tool down a little bit causing it to mill into the bed. It is also possible the collet that held the tool was damaged (now it certainly is), which caused my subpar tightening to become extra loose and cause the issue.

I switched into a 12mm tool to majorly reduce cut time and got cutting.

### Success, finally

{{< image src="12mmcut.jpg" alt="CNC cutting plywood">}} 12mm compared to 6mm is twice as fast! {{</ image>}}

After a fairly quick cutting I finally managed to get everything cut.

{{< image src="finally.jpg" alt="Four panels cut from a large piece of plywood">}} Some flaking, oh well {{</ image>}}

With this failed cut I can demonstrate the concept for paper. I will put a changable sheet of A4 paper into the pocket. Initially I planned to use an acrylic sheet to hold it in place using magnets but friction, tension and the "vacuum" behind the paper keep it in place.

{{< image src="testpaper.jpg" alt="A panel with an A4 paper embedded to it">}} Convinient {{</ image>}}

Then with a router I cut off the tabs from the pieces:

{{< image src="router.jpg" alt="4 panels and a router">}} Tabs are off! {{</ image>}}

Then I gave the panels a quick sanding before I get to spackling the grooves out:

{{< image src="sanding.jpg" alt="4 panels and a sander">}} Mr sandman, man me a sand... {{</ image>}}

Then I put some (a lot) spackle into all of the grooves and let it dry for a hot minute. Hot being I used a hairdryer/heatgun.

{{< image src="spackle.jpg" alt="Panels spackled">}} Looking worse so far... {{</ image>}}

After the spackle dried I sanded the parts more. After hours of sanding I got the panels to 600 grit. After sanding the surface was still flaking a little so I decided to put on some lacquer. It took me a while to combt through all of the spray cans we had available and find a suitable varnish. I ended up using a matte acrylic finish. Here's a pick of the parts drying:

{{< image src="varnished.jpg" alt="Panels drying">}} I like the yellowy tint from the lacquer {{</ image>}}

Now for some assembly. I screwed in some hinges I bought and also drilled four holes for four magnets (and glue).

{{< image src="magnets.jpg" alt="Close up of the side of a panel with magnets">}} Magnets, how do they work... {{</ image>}}

And finished! Finally.

{{< image src="tadaa.jpg" alt="Picture of the screen assembled">}} It came out real nice! {{</ image>}}

Here's a video of the screen as it's inteded to be used:

{{< video src="dmscreendemo.mp4" alt="Video of a man assebling the screen">}} This is how I use it {{</ video>}}
