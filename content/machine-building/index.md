---
title: Machine building
menu: project
weight: 9
---

#### The build

This week we were tasked with building a Prusa MINI+ 3D printer using a kit of parts. The whole process is documented in pictures at [This site](https://aaltofablab.gitlab.io/machine-building-2023-group-a).

#### My part

I was present for almost the entire two days it took to build the machine. Specifically I spent a lot of time figuring out what nuts and bolts go where and specialized in pushing square nuts into their holes with a tool and occasionally using a soft hammer to help the process. After the machine was done, I calibrated the machine and ran a test print which turned out great. Afterwards I've used the machine to print induction coil holders which you will see in the coming week.
