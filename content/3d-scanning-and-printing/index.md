---
title: 3D scanning and printing
menu: project
weight: 4
---

Here we go. 3D printing! When the course started I was the first in line to get the 3d printing permit so this week I hope I'll figure out something cool and new as I have been printing all kinds of stuff during the previous weeks.

### Measuring anisotropy

{{< blockquote author="Encyclopedia Britannica">}} Anisotropy, in physics, the quality of exhibiting properties with different values when measured along axes in different directions. Anisotropy is most easily observed in single crystals of solid elements or compounds, in which atoms, ions, or molecules are arranged in regular lattices. In contrast, the random distribution of particles in liquids, and especially in gases, causes them rarely, if ever, to be anisotropic.{{</ blockquote>}}

I've decided to measure the anisotropy in prints from the Ultimaker S3 FDM 3D printer. In short, I'm measuring the differences between the XZ plane (which I'll call the X axis) and Y axis of the printing. I chose this measurement because I had not previously heard about anisotropy and because it seemed a little less obvious of a test than infill, overhangs or bridges.

#### Settings

I'm printing the anisotropy measurement print on the Ultimaker S3 with standard red PLA filament. The filament is some kind of leftover piece so I'm minimizing waste. Here are my parameters:

- Layer height is set to standard 0.15mm. Its the quality I've been using for all of my prints.
- Infill is set to cubic subdivision. This is the filling I've been using after Youtube recommended it to me. Cubic subdivision has good strength in all directions and leaves air pockets to save filament.
- Build plate adhesion is achieved by heating the plate to 60 degrees. I use skirt to clean the nozzle and no brim.

After slicing the model with Cura I got to priting.

{{< image src="slice.jpg" alt="Slice of the anisotropy print in Cura">}} You got sliced - A hacker from the 90's {{</ image>}}

A solid printing time of 10 minutes. Here's a picture of what I made:

{{< image src="printedtest.jpg" alt="Picture of the printed anisotropy test model">}} I love the red filament. Its so vibrant. {{</ image>}}

#### Notes

The first difference between the X and Y axis is the surface finish. The top facing surface has a zig-zag texture with clearly defined borders whereas sideways facing surfaces have a line pattern and are smoother. There seems to be a seam at the point where the X and Y parts meet. I assume this is due to the plastic melting from the heat a little too much and thus sliding down a bit.

There seems to be a bit of variation but the thickness of the bottom part is about 2.5mm. The top part is about 2.65mm thick. I'm assuming the initial model had the same thickness for both parts. This means either the walls have expanded or the bottom is a little thinner probably because of the plastic melting due to the print bed heating. Both top and bottom seem to be exactly 5mm wide. The length of the piece is about 20.15mm and height about 19.90mm. This difference probably comes from the same melting that affects the thickness of the parts.

All edges are fairly sharp except those that exist on the XZ plane which are more round. These are the only edges the printer needs to specifically create as other edges come naturally from ending layers.

#### Conclusion

I'm fairly happy with these results. The 0.15mm layer thickness gives fairly good quality for the time. The differences in dimensions are negligeble in models that do not need perfect precision. The melted steam is a little annoying but I don't think you can easily get rid of it. Perhaps without bed heating and for example using PVA glue as adhesion you could avoid this issue.

### Designing a small object

The requirement for this design is that it cannot be made with subtractive methods. I came up with a one directional valve that should shut when water goes through it in one direction and allow water to pass in the other.

I started sketching in FreeCAD and here is what I ended up with. Initially I thought about doing a ball as ball valves are the way these things usually are made but I decided to use a cone shape instead to make sure the printer can handle it.

Here are some initial design sketches.

{{< image src="inside.jpg" alt="Frist sketch of the inside part">}} Now we revolve {{</ image>}}

{{< image src="outside.jpg" alt="First sketchg of the outside part">}} This revolves around the x axis too {{</ image>}}

{{< image src="in3d.jpg" alt="Picture of the sketch in 3D">}} And now we print. {{</ image>}}

{{< video src="printsample.mp4" alt="Slice of the print in Cura">}} This is how it forms {{</ video>}}

{{< image src="finished.jpg" alt="Picture of the first print finished">}} Its adorable {{</ image>}}

The print failed. I tried to save resources but the gap was too small so all parts fused together.

Let's try this instead:

{{< image src="newversion.jpg" alt="Picture of the second version in 3D">}} Here we go again {{</ image>}}

This design kept failing in the printer. I assume the central circle is too small and won't adhere properly to the build plate.

{{< image src="adhesionfailure.jpg" alt="Picture of the second version printed and failed">}} I aborted the print when I noticed the central piece failed. {{</ image>}}

Back to CAD. I made the central piece larger and moved the holes to the outside instead of the inside.

{{< video src="finalslice.mp4" alt="Video of the final version sliced in Cura">}} Here is the final design. {{</ video>}}

This printed out nice:

{{< video src="demo.mp4" alt="Demo of the final print in my hand">}} Yay it printed (mostly) successfully {{</ video>}}

Despite the printer printing really bad quality the print finished and the central part moves independently. The thing works, at least in theory. In actuality the central plug is not watertight or the poor print quality left holes in the thing so it does not actually hold water but from these tests you can kind of see that the design works as inteded. Fortunately this was never supposed to be a functional product but moreso a challenge for me to design.

{{< video src="demowater.mp4" alt="Demo of the print in water.">}} Doesn't actually work but I deem this a success {{</ video>}}
