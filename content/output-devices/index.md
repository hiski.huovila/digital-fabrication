---
title: Output devices
menu: project
weight: 8
---

This week continues from the previous week of electronics production. The Neopixels (or in this case an offbrand alternative) arrived so I could solder in the LED. The soldering itself did not pose any issues. It seems like I've figured the process out. Woooo!

There was one issue though: Some pins had again the continuity issue of last week. After some scraping and frustrations I asked for help in removing the pixel to resolder any problematic connections but instead turns out I my measurements were perhaps too precise. Changing settings in the multimeter showed me that the minimal amount of current that would flow through is not noticable enough to cause any issues. Yay!

{{< image src="pixel.jpg" alt="PCB with a powered on adressable LED">}} It's working! It's working! {{</ image>}}

I repurposed my code from Embedded programming week, did some tweaks and ended up with this:

```c
#include <Adafruit_NeoPixel.h>

#define NUMPIXELS 1

int neoout = 26;
int neoin = 27;
int led = 28;
int button_pullup = 29;
int fourpin1 = 2;
int fourpin2 = 3;
int fourpin3 = 4;

int smallpixel_power = 11;
int smallpixel_in  = 12;
float hue = 0;
bool state = true;
bool wasopen = true;

Adafruit_NeoPixel bigpixel(NUMPIXELS, neoin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel smallpixel(NUMPIXELS, smallpixel_in, NEO_GRB + NEO_KHZ800);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  pinMode(button_pullup, INPUT_PULLUP);
  bigpixel.begin();
  bigpixel.setBrightness(10);
  bigpixel.clear();

  pinMode(smallpixel_power, OUTPUT);
  digitalWrite(smallpixel_power,HIGH);

  smallpixel.begin();
  smallpixel.setBrightness(10);
  smallpixel.clear();

  pinMode(PIN_LED_G,OUTPUT);
  pinMode(PIN_LED_R,OUTPUT);
  pinMode(PIN_LED_B,OUTPUT);
  
  digitalWrite(PIN_LED_R, LOW);
  digitalWrite(PIN_LED_G,HIGH);
  digitalWrite(PIN_LED_B,HIGH);

}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    String str = Serial.readString();
    hue = str.toFloat();
    Serial.println("Changing to: ");
    Serial.println(hue);
    if(hue < 0 || hue > 360) {
    Serial.println("Set hue between 0 and 360, hue set to 0");
    hue = 0;
    }
  }
  
  //button is open
  if(digitalRead(button_pullup) == HIGH) {
      if(!wasopen) {
        wasopen = true;
      }
      if(state){
        bigpixel.clear();
        smallpixel.clear();
        float to16bit = (hue / 360.0) * 65535.0;
       
        bigpixel.setPixelColor(0,bigpixel.ColorHSV(to16bit,255,255));
        bigpixel.show();
       
        smallpixel.setPixelColor(0,bigpixel.ColorHSV(to16bit,255,255));
        smallpixel.show();

        digitalWrite(PIN_LED_R, HIGH);
        digitalWrite(PIN_LED_G,HIGH);
        digitalWrite(PIN_LED_B,HIGH);
      
       if( 60 > hue && hue >= 0){
         digitalWrite(PIN_LED_R,LOW);
       } else if( 180 > hue && hue >= 60 ){
         digitalWrite(PIN_LED_G,LOW);
       }
       else if( 300 > hue && hue >= 180 ){
         digitalWrite(PIN_LED_B,LOW);
       }
       else if( 360 >= hue && hue >= 300){
         digitalWrite(PIN_LED_R,LOW);
       }

      } else {
        digitalWrite(PIN_LED_R, HIGH);
        digitalWrite(PIN_LED_G,HIGH);
        digitalWrite(PIN_LED_B,HIGH);
        
        bigpixel.clear();
        bigpixel.show();
        
        smallpixel.clear();
        smallpixel.show();
      }
  }
  //button is closed
  else {
    if(wasopen){
      state = !state;
      wasopen = false;
    }
    }
}
```

The device now lights up all 3 of its LED's and their color can be changed via sending a value between 0 and 360 via the serial port. The button now works perfectly, turning on and off all 3 LED's at the same time. And you can even hold it down without causing issues!

{{< video src="hero.mp4" alt="Small electronic device with color changing led's">}} Full RGB control! {{</ video>}}

After this I decided to add a four digit display to display the current hue as a number. Unfortunately the display used 5V power and the 2x2 connector I soldered before could only supply 3.3V of power. So I had to solder in an extra wire to get the display attached.

{{< image src="jump.jpg" alt="PCB with an extra cable soldered to a pin">}} Go ahead and jump. Jump! {{</ image>}}

With the following modifications to the code, the display now works wonderfully. I had to do a bit of a hack to get the display to show 0 instead of being empty but this was the fastest method. The code will produce a warning that it does not follow ISO C++ standard but that can be ignored for this quick project.

```c
#include <Adafruit_NeoPixel.h>
#include <TM1637.h>

#define NUMPIXELS 1

int neoout = 26;
int neoin = 27;
int led = 28;
int button_pullup = 29;

int smallpixel_power = 11;
int smallpixel_in  = 12;
float hue = 0;
bool state = true;
bool wasopen = true;

Adafruit_NeoPixel bigpixel(NUMPIXELS, neoin, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel smallpixel(NUMPIXELS, smallpixel_in, NEO_GRB + NEO_KHZ800);

const int CLK = 2;
const int DIO = 4;
TM1637 tm1637(CLK, DIO);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps

  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);

  pinMode(button_pullup, INPUT_PULLUP);
  bigpixel.begin();
  bigpixel.setBrightness(10);
  bigpixel.clear();

  pinMode(smallpixel_power, OUTPUT);
  digitalWrite(smallpixel_power,HIGH);

  smallpixel.begin();
  smallpixel.setBrightness(10);
  smallpixel.clear();

  pinMode(PIN_LED_G,OUTPUT);
  pinMode(PIN_LED_R,OUTPUT);
  pinMode(PIN_LED_B,OUTPUT);
  
  digitalWrite(PIN_LED_R, LOW);
  digitalWrite(PIN_LED_G,HIGH);
  digitalWrite(PIN_LED_B,HIGH);

  pinMode(CLK,OUTPUT);
  pinMode(DIO,OUTPUT);

  tm1637.init();
  tm1637.set(BRIGHTEST);
  Digitdisplay();
}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    String str = Serial.readString();
    hue = str.toFloat();
    Serial.println("Changing to: ");
    Serial.println(hue);
    if(hue < 0 || hue > 360) {
      Serial.println("Set hue between 0 and 360, hue set to 0");
      hue = 0;
    }
    Digitdisplay();
  }
  
  //button is open
  if(digitalRead(button_pullup) == HIGH) {
      if(!wasopen) {
        wasopen = true;
      }
      if(state){
        bigpixel.clear();
        smallpixel.clear();
        float to16bit = (hue / 360.0) * 65535.0;
       
        bigpixel.setPixelColor(0,bigpixel.ColorHSV(to16bit,255,255));
        bigpixel.show();
       
        smallpixel.setPixelColor(0,bigpixel.ColorHSV(to16bit,255,255));
        smallpixel.show();

        digitalWrite(PIN_LED_R, HIGH);
        digitalWrite(PIN_LED_G,HIGH);
        digitalWrite(PIN_LED_B,HIGH);
      
       if( 60 > hue && hue >= 0){
         digitalWrite(PIN_LED_R,LOW);
       } else if( 180 > hue && hue >= 60 ){
         digitalWrite(PIN_LED_G,LOW);
       }
       else if( 300 > hue && hue >= 180 ){
         digitalWrite(PIN_LED_B,LOW);
       }
       else if( 360 >= hue && hue >= 300){
         digitalWrite(PIN_LED_R,LOW);
       }

      } else {
        digitalWrite(PIN_LED_R, HIGH);
        digitalWrite(PIN_LED_G,HIGH);
        digitalWrite(PIN_LED_B,HIGH);
        
        bigpixel.clear();
        bigpixel.show();
        
        smallpixel.clear();
        smallpixel.show();
      }
  }
  //button is closed
  else {
    if(wasopen){
      state = !state;
      wasopen = false;
    }
    }
}

void Digitdisplay() {
    if(hue == 0){
      tm1637.displayStr("   0");
    }
    else {
    tm1637.displayNum(hue);
    }
    delay(50);
}
```

And here is the demo video:

{{< video src="displayvid.mp4" alt="Display displays multiple numbers in succession">}} Check out dis display! {{</ video>}}

After that I measured the power consupmption of the LCD display. The operating voltage of the display, while plugged in to my computer, is 5.12V. Turns out if you disconnect the 5V pin from the display, it still works, just not as bright as I run it at maximum brightness. With the multimeter I measured 35mA of current using  the following circuit:

{{< image src="current.jpg" alt="PCB with a multimeter connected to it">}} In current news: 35mA! {{</ image>}}
