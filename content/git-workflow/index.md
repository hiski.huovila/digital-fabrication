---
title: Git workflow
menu: website
---

This website demands version control and subsequent projects might too. I have plenty of git experience on git so I intend to write this page as a documentation some of my peers might be able to use.

#### Git is software

Git is a version control software installed on a computer. It stores versions of files in a local `repository`. Any change made to a git repository is stored as a `commit`.

#### Gitlab is a service

Gitlab (gitlab.com) is an online service that stores git commits on a remote server. Gitlab also publishes and hosts this website on their server (unless I move this elsewhere).

### Starting a new project

Personally I like to first create the repository in Gitlab using the new repostory button. It is a good idea to use the *initialize the project with a README.md* tab to have at least one file in the repository.

#### SSH stuff

The SSH aka secure shell protocol is a joint agreement between two computers on how to communicate with each other. To establish and ssh connection you would use the `ssh` command but it is not needed when using git. To set up ssh with Gitlab (or any other computer ) I use

``` bash
ssh-keygen
```

I tend to leave the password empty though I probably should lock the ssh key with a password for extra safety. The command will generate two files to your filesystem. The default location is the hidden `.ssh` directory usually in root. There you will find the private key `id_rsa` and the public key `id_rsa.pub` (and the `known_hosts` file which can be ignored).

##### id_rsa

id_rsa is a file which contains a key to your computer. This key is used to scramble any data you send and unscramble data you receive. This key is very important and should never be shared anywhere because it gives access to your computer.

##### id_rsa.pub

id_rsa.pub is a sister file to the previously mentioned id_rsa. It is the public version of your private key which you send to anyone you want to connect to your computer. They will use the public key to scramble data they send you and unscramble data you send them. This creates a secure connection no-one else can interrupt. This also acts as a strong verification (but not actual strong verification) of your and their identity because you would only give the public key to those you trust.

#### Back to Gitlab

Now I add my public key to the Gitlab shh keys. If the computer is not my personal one I will add an expriration date.

Then I can clone the repository using

``` bash
cd <the location I want the repository>
git clone <git url>
```

### Making changes

If I was working with other people or multiple computers I would first run `git pull` to get all changes that have been made.

Every time I add a file I run

```bash
git add <filename>
```

Usually I tend to use `git add .` which adds all new files BUT this should only be used if you know all of the files you add. I tend to create a `.gitignore` file if I want to exclude stuff from the repository.

### Making a change

Any change (adding files too) needs to be saved to the repository. This I do with

```bash
git commit -m "<some kind of message>"
```

The changes need to then be pushed online which I do using

```bash
git push
```

That is all. For now there is no need for branching or merge conflicts because I will work on this repository alone and without major version changes. If I later make a python script or some other complicated software system I will make branches for new changes.
