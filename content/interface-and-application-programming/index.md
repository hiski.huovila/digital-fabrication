---
title: Interface and application programming
menu: project
weight: 13
---

This week we are tasked to build a website that can control the board we made last week. The assignment lets us use any framework we want and I inteded to use React but due to time constraints I decided to build the site with jquery to get it done quickly. Time to fire up chatGPT and see how fast I can get this running.

#### What I asked

I want to build a website using html, css and javascript using the jquery library and the bootstrap css framework. On the site I want to be able to input a color via a color wheel using hue, saturation and value. There should also be a button and a way to show whether a REST api is connected or not. Could you write me step by step instructions on how to do so?

#### ChatGPT responds

Sure, here are the step-by-step instructions on how to build a website using HTML, CSS, and JavaScript with jQuery and Bootstrap, including an input for color selection, a button, and a way to show whether a REST API is connected or not.

Step 1: Set up the basic HTML structure

Start by creating an HTML file and set up the basic structure of the page, including a head section for the CSS and JavaScript files, and a body section for the content.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>My Website</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container">
      <h1>My Website</h1>
      <!-- Content goes here -->
    </div>
  </body>
</html>
```

Step 2: Add the color input using a color wheel

To allow users to select a color using a color wheel, we'll use the Spectrum jQuery plugin. First, we need to download the plugin and include it in the head section of our HTML file:

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
```

Next, we can add the color input to the page by creating an input element with the type "text" and adding the "spectrum" class to it. This will automatically turn the input into a color picker:

```html
<div class="form-group">
  <label for="color">Select a color:</label>
  <input type="text" id="color" class="form-control spectrum" />
</div>
```

Step 3: Add a button and a way to show API connection status

To add a button, we can create a button element with the "btn" and "btn-primary" classes, and add an onclick event that will trigger a function when the button is clicked:

```html
<button id="submit" class="btn btn-primary" onclick="submitColor()">Submit</button>
```

To show whether the REST API is connected or not, we can create a div element with an ID of "api-status" and update its text content depending on the API connection status:

```html
<div id="api-status"></div>
```

Step 4: Write the JavaScript code

Now that we have the HTML structure in place, we can write the JavaScript code that will handle the color selection, button click, and API connection status.

First, we need to initialize the color picker using the following code:

```html
$(document).ready(function() {
  $("#color").spectrum({
    preferredFormat: "hex",
    showInput: true
  });
});
```

This will initialize the color picker with the preferred format set to hexadecimal and show an input box for users to type in the color value if they prefer not to use the color wheel.

Next, we can write the submitColor function that will be called when the button is clicked. This function will get the selected color value and send it to the API. We'll assume that the API endpoint for submitting colors is "/api/submit-color":

```html
function submitColor() {
  var color = $("#color").spectrum("get").toHexString();
  $.post("/api/submit-color", {
```

#### Then I asked

Could you then write everything in a single hmtl file using the instructions you gave?

#### chatGPT responded with

Certainly! Here is the complete HTML file that includes the instructions from my previous response:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>My Website</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <script>
      $(document).ready(function() {
        $("#color").spectrum({
          preferredFormat: "hex",
          showInput: true
        });
      });

      function submitColor() {
        var color = $("#color").spectrum("get").toHexString();
        $.post("/api/submit-color", {
          color: color
        }, function(data, status) {
          if (status === "success") {
            $("#api-status").text("API connected");
          } else {
            $("#api-status").text("API not connected");
          }
        });
      }
    </script>
  </head>
  <body>
    <div class="container">
      <h1>My Website</h1>
      <div class="form-group">
        <label for="color">Select a color:</label>
        <input type="text" id="color" class="form-control spectrum" />
      </div>
      <button id="submit" class="btn btn-primary" onclick="submitColor()">Submit</button>
      <div id="api-status"></div>
    </div>
  </body>
</html>
```

Note that you'll need to replace the API endpoint URL in the submitColor function with the correct URL for your API. Also, make sure to include any necessary authentication or other parameters in the POST request.

#### Now for some reflection

Acting as a professional frontend developer on stack overflow could you go through your code and make sure there are no mistakes?

#### chatGPT responds

As an AI language model, I'm not able to access Stack Overflow or provide professional services as a developer. However, I can review the code and provide some general feedback.

Looking at the code, I don't see any obvious syntax errors or issues with the HTML, CSS, or JavaScript code. However, I do notice that the spectrum.min.css and spectrum.min.js files are being loaded from a CDN, which could potentially cause issues if the CDN goes down or if there are network connectivity issues.

It's also important to note that the code assumes that the API endpoint URL and authentication parameters are correctly configured, and that the server is properly handling the POST request with the color parameter. These factors are outside the scope of the provided code and should be double-checked to ensure proper functionality.

Overall, the code looks sound, but as with any code, there may be additional considerations and optimizations that are specific to the use case and environment.

### Actual coding

{{< image src="generated.jpg" alt="A website with a color picker and a button">}} The site made by chatGPT {{</ image>}}

Now was time for me to adapt chatGPT code to suit my needs. I decided to use the provided example code (which was also initially from chatGPT) for the arduino side. I had major issues trying to get json parsed and attached to the body of the GET request so I decided to hack my way through the problem by sending the json data as a string parameter. This is definitely not the way I should do this but due to time constraints this is what we'll have to work with.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>LED color control</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <script>
      $(document).ready(function() {
        $("#color").spectrum({
          preferredFormat: "hsl",
          showInput: true
        });
      });

    function submitColor() {
        var color = $("#color").spectrum("get").toHsl(); // {"h":0,"s":1,"l":0.5}

        $.ajax({
                url: "http://193.167.5.160/json?string={\"h\":"+ color.h +",\"l\":"+ color.l +"}",
                timeout: 5000,
            })
            .done(function(response){
                const responseJSON = JSON.parse(response);
                console.log(responseJSON);
            })
            .fail(function(){
                console.log("Sorry, call failed");
            });
    }

    function ping() {
        $.ajax({
            url: "http://193.167.5.160/json",
            timeout: 5000,
        })
        .done(function(response){
            $('#api-status').html("Connected");
        })
        .fail(function(){
            $('#api-status').html("Disconnected");
        });
    }

    setInterval(ping, 5000);
    </script>
  </head>
  <body>
    <div class="container">
      <h1>LED color control</h1>
      <div class="form-group">
        <label for="color">Select a color:</label>
        <input type="text" id="color" class="form-control spectrum" />
      </div>
      <button id="submit" class="btn btn-primary" onclick="submitColor()">Submit</button>
      <div id="api-status">
        Searching
      </div>
    </div>
  </body>
</html>
```

I ran into an issue. I managed to get the REST api working but the website would not get an accepted response and the data would not go through. Essentially I could use the api in a development environment but not in Firefox. Eventually I identified the issue to be that the response my REST api gives did not have proper headers attached. So the following code contais the necessary cross-origin policy headers for me to be able to control the board from a deployed website. This most likely will end up being a security risk but fortunately my REST api will not be constantly online. In the future I would probably use another library that would also grant me HTTPS connection instead of HTTP for added security.

```c
#include <Adafruit_NeoPixel.h>
#include <TM1637.h>
#include <Arduino.h>
#include <WiFi.h>
#include <ESPAsyncWebSrv.h>
#include <AsyncJson.h>
#include <ArduinoJson.h>

#define NUMPIXELS 1

// Some variables we will need along the way
const char* ssid     = "Fablab";
const char* password = "Fabricationlab1";   
const char* PARAM_MESSAGE = "message"; 
int webServerPort = 80;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

//hardware pins
int neoout = D0;
int neoin = D1;
int led = D2;
int button_pullup = D3;

//variables
float hue = 0;
bool state = true;
bool wasopen = true;

Adafruit_NeoPixel bigpixel(NUMPIXELS, neoin, NEO_GRB + NEO_KHZ800);

//display setup
const int CLK = D8;
const int DIO = D9;
TM1637 tm1637(CLK, DIO);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps

 delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Serve the JSON payload
 server.on("/json", HTTP_GET, [](AsyncWebServerRequest *request) {

   if (request->hasParam("string")) {
      // The incoming params are Strings
    String param = request->getParam("string")->value();
    
    // Allocate a JSON buffer and parse the payload
    DynamicJsonDocument doc(1024);
    deserializeJson(doc, param);

    // Access the data in the JSON object
    float h = doc["h"];
    float l = doc["l"];

    hue = h;

    if(hue < 0 || hue > 360) {
      hue = 0;
    }
    
    Digitdisplay();
    bigpixel.setBrightness(l * 100);
    // Print the data to the serial monitor
    Serial.print("Hue: ");
    Serial.println(hue);
    Serial.print("Brightness: ");
    Serial.println(bigpixel.getBrightness());

    // Send the JSON payload as the HTTP response
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", "{\"hue\":" + String(hue) +", \"brightness\":" + String(bigpixel.getBrightness()) + "}");
    response->addHeader("Access-Control-Allow-Origin", "*");
    response->addHeader("Access-Control-Allow-Methods","*");
    response->addHeader("Cross-Origin-Resource-Policy", "cross-origin");
    request->send(response);
   } else {
     // Send the JSON payload as the HTTP response
    AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", "Use parameter string for a response");
    response->addHeader("Access-Control-Allow-Origin", "*");
    response->addHeader("Access-Control-Allow-Methods","*");
    response->addHeader("Cross-Origin-Resource-Policy", "cross-origin");
    request->send(response);

   }
});

  // Start the server
  server.begin();

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);

 // hardware stuff
  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);

  pinMode(button_pullup, INPUT_PULLUP);
  bigpixel.begin();
  bigpixel.setBrightness(10);
  bigpixel.clear();

  pinMode(CLK,OUTPUT);
  pinMode(DIO,OUTPUT);

  tm1637.init();
  tm1637.set(BRIGHTEST);
  Digitdisplay();
}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    String str = Serial.readString();
    hue = str.toFloat();
    Serial.println("Changing to: ");
    Serial.println(hue);
    if(hue < 0 || hue > 360) {
      Serial.println("Set hue between 0 and 360, hue set to 0");
      hue = 0;
    }
    Digitdisplay();
  }
  
  //button is open
  if(digitalRead(button_pullup) == HIGH) {
      if(!wasopen) {
        wasopen = true;
      }
      if(state){
        bigpixel.clear();
        float to16bit = (hue / 360.0) * 65535.0;
        bigpixel.setPixelColor(0,bigpixel.ColorHSV(to16bit,255,255));
        bigpixel.show();
      }
      else {
        bigpixel.clear();
        bigpixel.show();
      }
  }
  //button is closed
  else {
    if(wasopen){
      state = !state;
      wasopen = false;
    }
    }
}

void Digitdisplay() {
    if(hue == 0){
      tm1637.displayStr("   0");
    }
    else {
    tm1637.displayNum(hue);
    }
    delay(50);
}   
```

{{< video src="demo.mp4" alt="Working site demo video">}} Its working {{</ video>}}

You can see the control website [here](website.html)
