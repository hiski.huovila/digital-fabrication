---
title: Input devices
menu: project
weight: 10
---

This week we were tasked to add some input to the Xiao board. I decided to further my final project idea and build a wireless power transfer circuit and measure its output, using the measurement as input to the Xiao.

#### The first coil

I designed this simple spool to wrap a wire around and create an induction coil.

{{< image src="coil.jpg" alt="3d model of a spool">}} Simple, yet effective {{</ image>}}

Then I quickly printed the spool with the Prusa MINI+ 3D printer we built last week.

{{< image src="spool.jpg" alt="3d printed spool">}} Adorable isn't it? {{</ image>}}

Then I wrapped around some magnet wire.

{{< image src="1stcoil.jpg" alt="Magnet wire around a spool">}} Now lets get this thing powered. {{</ image>}}

#### The AC problem

To get induction in the coil I built I needed alternating current that would create a magnetic field in another coil. This proved difficult as I could not find an AC power supply that I could use safely in the lab. Notably all wall outlets give the standard 230V AC power that all electronics use but I needed maybe up to 5V of power.

I figured out the Xiao board can use `analogWrite` to create a pulsing signal. With this code I got exactly half of the voltage of the Xiao board in AC.

```c
int pin = 26;
void setup() {
  // put your setup code here, to run once:
  pinMode(pin, OUTPUT);
  analogWrite(pin, 127);
}
```

{{< image src="acxiao.jpg" alt="Voltimeter showing 1.7V of current">}} What now? {{</ image>}}

#### The transistor story

I realized I cannot reasonably both send current from the Xiao board and also measure with it so I decided to build a wireless charger. After some reseach I settled on [this](https://edisonsciencecorner.blogspot.com/2022/01/diy-wireless-leds.html) schematic from Edisonsciencecorner's Youtube [video](https://www.youtube.com/watch?v=q_ZMNtrwij0&ab_channel=EDISONSCIENCECORNER).  

I did not have the exact transistor so I found another one with more power ratings and gave it a try. I also built a new coiled LED from this big LED I found lying around.

{{< image src="betterLED.jpg" alt="A big LED with a coil soldered to it">}} This big boi can be powered with 2V {{</ image>}}

Now to measure

{{< image src="measuring.jpg" alt="A big LED with a coil inside another coil">}} The setup {{</ image>}}

{{< image src="powersupply.jpg" alt="DC power supply with 2mA of current and 5V">}} The power {{</ image>}}

{{< image src="someinduction.jpg" alt="Small voltage in an osciloscope graph">}} The result {{</ image>}}

The LED did not power on but the oscilloscope measured about half a volt. I tried turning up the power but just managed to burn two transistors. Eventually I found a transistor that was nearly identical to the one used in the schematic but it did not help.

#### Are you have stupid?

After about 10 hours of testing and trying I figured out what was wrong. The schematic has two coils on top of each other and a transistor that alternates between them. This creates a changing magnetic field needed for induction. I had brilliantly changed direction when making the coil essentially negating the effect. After making a new coil I finally got 1.5V of electricity. Still no light though.

#### Success

After some calculations I figured out that the coil I had the LED connected to was too small. So I printed a new bigger spool with exactly the same inductance as the coil in the schematic. And that coil got a peak voltage of 5.5V.

{{< image src="volts.jpg" alt="Oscilloscope graph with 5.5V">}} Now that is power {{</ image>}}

Which is more than enough to power an LED!!!

{{< image src="working.jpg" alt="LED powered by induction">}} Now we party {{</ image>}}

With the following code in the Xiao board I could begin measuring voltage.

```c
int pin = A0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(pin, INPUT);
}

void loop() {
  int value = analogRead(pin);
   if (Serial.available() > 0) {
    // read the incoming byte:
  float calculated = (value / 1023.0) * 3.3;
    Serial.println(calculated);
    }
}
```

{{< image src="measuringsetup.jpg" alt="Xiao board connected to an LED">}} Lets see what is going on {{</ image>}}

This measurement can be  plotted into a graph:

{{< image src="graphvoltage.jpg" alt="A graph with some spikes">}} 2.5V {{</ image>}}

It seems the Xiao board gets about half of the AC and the other half goes to the LED. It's worth noting that the graph shows only spikes as the induced AC goes to the peak only once per cycle. The LED also prevents the power from going in two directions so we can measure only half of the power. The Xiao board is measuring DC but we are working with AC so we could not measure the full AC power anyway.

This is a great success and a good trial run for my upcoming final project with wireless LED's.
