---
title: Wildacard week
menu: project
weight: 14
---
Wildcard week! This week we are allowed to use any kind of manufacturing technique available in Aalto as long as it is not something we already do. I've chosen to do injection molding as my project as I want to mass produce small boardgame pieces.

### CAD stuff

Is started out designing a mold like this

{{< image src="moulda.jpg" alt="FreeCAD design of a mold">}} 1st part {{</ image>}}

{{< image src="mouldb.jpg" alt="FreeCAD design of a mold">}} 2nd part {{</ image>}}

Which fit together nicely like this

{{< image src="mouldclosed.jpg" alt="FreeCAD design of a mold">}} Together {{</ image>}}

Then I 3D-printed the mold with high-temperature resin so it can withstand the high heat of injected plastic.

{{< image src="printrender.mp4" alt="Video of mold print simulation">}} This is how they look {{</ image>}}

{{< image src="printready.jpg" alt="mold parts in preform ready to print">}} Let's go {{</ image>}}

{{< image src="curing.jpg" alt="Resin print curing">}} Wash time 6 minutes - curing time 90 minutes {{</ image>}}

{{< image src="done.jpg" alt="FreeCAD design of a mold">}} Curious color {{</ image>}}

Unfortunately due to some design flaws (mainly the alignment pins were too small and did not have enough tolerance) the mold broke. This was not too bad as I already wanted to make another mold with better design.

Here is the second version.

{{< image src="mouldv2a.jpg" alt="FreeCAD design of a mold">}} 1st part {{</ image>}}

{{< image src="mouldv2b.jpg" alt="FreeCAD design of a mold">}} 2nd part {{</ image>}}

The M5 holes that were supposed to hold the mold in place failed in the print so I quickly laser cut this mold holder that could slot M5 nuts. It worked surprisingly well.

{{< image src="newmould.jpg" alt="Resin printed mold in a plywood casing">}} Some quick improvization {{</ image>}}

### (almost) Injecting

Here we are setting up the injection molding machine.

{{< image src="settingup.jpg" alt="Injection molding machine">}} Fixing the mold in place{{</ image>}}

{{< image src="readytoinject.jpg" alt="molds affixed to the machine">}} Ready to inject {{</ image>}}

The plastic granules enter from the top and then we push.

{{< image src="injecting.jpg" alt="Plastic going in to the machine">}} Injection in progress {{</ image>}}

Unfortunately the mold does not work. The sprue does not properly close the channel. Due to this the plastic does not get injected into the mold but instead squeezes out of the side of the sprue. Further design and development is needed.

To be continued at a later date...
