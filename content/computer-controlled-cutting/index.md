---
title: Computer controlled cutting
menu: project
weight: 2
---

Now with some CAD knowledge under my belt it's time to set out and make something with the drawings. I grew frustrated with the limited selection of laser cut TTRPG terrain so I decided to make my own. I'm constraining myself to these limitations: Price has to be less than commerial products, preferably the price of a single sheet of plywood. Design must take advantage of limited materials to preserve storage space. Everything must be usable with a 1-inch grid.

A big issue with commercial laser cut terrain is how detailed it is. I wanted to purchase a single set of building blocks I could use for all kinds of wild encounters, not just dungeons or forests. This is why I've decided to design everything fairly plain. Boxes that can be used as crates, rocks, water buckets, wells etc. A bridge that doubles as a boat on the other side. Stairs that can use the supporting triangle as a campfire.

Here's a sketch.

{{< image src="sketch.jpg" alt="Hand drawn sketches of the project">}} Some ideas {{</ image>}}

I got to drawing with SolveSpace. Here's what I initially came up with

{{< image src="1stcad.jpg" alt="SolveSpace sketch of the project">}} Universal terrain? Dungeon stilts? {{</ image>}}

Then I wanted to make platforms, so I drew this. It took a wile to iterate in my head about the connection mechanism but I decided to try out these square pegs.

{{< image src="stilts.jpg" alt="SolveSpace sketch of dungeon stilts">}} Now lets get cutting {{</ image>}}

I exported the file as a `.svg` file (a vector format) and it came out a little weird. I hid pretty much everything in SolveSpace and exported the view and managed to get just the lines out. Now I set the lines to 0.01mm and I could try cutting this thing. Here's a pick from Inkscape.

{{< image src="stiltssvg.jpg" alt="SolveSpace sketch of dungeon stilts in Inkscape">}} Inkscape cleanup, lines are not at actual size {{</ image>}}

Then I put it into Illustrator, checked the linesizes and started cutting.

{{< image src="illustrator.jpg" alt="Adobe Illustrator opening the svg file">}} You can clearly see how hard it is to see the thin lines. {{</ image>}}

The initial prototype assumed that the cardboard was 2mm thick (it was 3mm). The first prototype didn't come off from the board because everything was so small and the air pockets really reduced structural integrity.

{{< image src="cuttingcardboard.jpg" alt="Cardboard in the laser cutter">}} Initially looking good. {{</ image>}}

{{< image src="toosmall.jpg" alt="Cardboard out of the laser cutter and failed cuts">}} The only part that came off the cardboard. {{</ image>}}

Now that I had realized 2mm plywood would have way too small pegs to be functional I decided to give it another go, changed everything to 5mm size and cut it from plywood,

{{< image src="nowwithply.jpg" alt="Same cuts but from 5mm plywood">}} Now with plywood {{</ image>}}

However the power was not enough and it didn't cut through fully. I had to manually cut off the excess wood to get the parts out.

{{< image src="notenoughpower.jpg" alt="Plywood cut with laser but it didn't go through">}} The bottom right corner of the machine does not give enough power {{</ image>}}

Now I tested these parts. I knew that without taking into account kerf the parts would be loose but I wanted to know how loose. Here is the result.

{{< video src="itsloose.mp4" alt="Plywood cut with laser but it didn't go through">}} The bottom right corner of the machine does not give enough power {{</ video>}}

I really wanted to avoid dealing with kerf because I would like to share these files and let anyone build my design. I toyed with the idea of using 4M bolts and nuts with 4mm holes in the corners to make adjustable legs and I might design that at some point but the assignment wanted us to make snap fitting joints so eventually I calculated the kerf and fixed my files to take that into account. What is kerf? Read about how to calculate kerf [here](https://aaltofablab.gitlab.io/laser-cutting-2023-group-a/kerf.html).

As you can see in the following picture, the 10mm wide part is actually 9.6mm wide. This means the laser cut an extra 0.2mm from both sides of the part.

{{< image src="nokerf.jpg" alt="Measuring plywood part thickness">}} Too thin. Too bad. {{</ image>}}

I cut the parts now with 0.2mm kerf (it took me a couple trys to get the 0.2mm implemented properly). Howver either due to mistake in my calculation or some other factor I got the outside kerf fixed and managed to cut a 5mm wide peg but it did not fit. Turns out the inside square of holes was only 4.8mm wide. Instead of fixing that I realized the 5mm plywood is not exactly 5mm and might be as thin as 4.8mm in certain parts so I decided to instead reduce the peg size to 4.8mm wide (because otherwise it would be 5x4.8 and not be a square). This 4.8mm peg worked with  the other parts perfectly and my prototype came out really nice.

{{< image src="gotit.jpg" alt="A working prototype">}} Its working WOOOOO! {{</ image>}}

Now with my burnt smelling plywood and about 30 minutes of time left I quickly went home to prepare for my Dungeons and Dragons game to test this product. The product worked really well, the game went really well and I was happy.

{{< image src="itsworking.jpg" alt="A game of DND in progress">}} Perfect {{</ image>}}

Inspired by the global lectures I decided to make a last minute addtion: Small chamfers at the ends of the pegs to improve fit.

{{< image src="chamfer.jpg" alt="Chamfers added in SolveSpace">}} Nice little chamfers to make the pegs easier to use {{</ image>}}

### FreeCAD vs SolveSpace continues

As mentioned previously I decided to try out FreeCAD and redo the project. Here are my thoughts.

{{< image src="freecadfirst.jpg" alt="A white rectangle in freeCAD">}} That took way too long {{</ image>}}

First time opening I was very confused. How do I begin drawing? Fortunately I found the draft field and figured out how to draw. It took me way too long to figure out how to get a rectangle visible. Turns out the draft tool is not similar to SolveSpace at all and I eventually found myself at the part design sketch tool. Finally something I understand.

{{< image src="freecadsecond.jpg" alt="Beginning of the project in FreeCAD">}} Here we go {{</ image>}}

I did manage to get all of the important parts sketched and I did really like the mirror tool but it honestly was not faster or easier or better at all. Perhaps for 3D part design FreeCAD will be a lot better than SolveSpace but for some simple 2D designs SolveSpace is just fine. Here's the final pic:

{{< image src="freecadthree.jpg" alt="The project finalized in FreeCAD">}} The same dnd platfrom from next week (mostly) but it looks a lot less sci-fi {{</ image>}}

### Tide begins to turn

I now needed a 3d model of the design. I decided to redraw everything (twice actually) in FreeCAD and I'm starting to change my mind.

{{< image src="extrusion.jpg" alt="FreeCAD drawing extruded to 3D">}} It looks real ugly {{</ image>}}

The first extrusion didn't come out very nice but over time I began learning the tool and getting things done.

{{< image src="freecad3d.jpg" alt="The design extruded to 3d properly">}} Now we are talking {{</ image>}}

Eventually I figured out my way in FreeCAD and got this lovely 3D model out:

{{< image src="3drender.jpg" alt="Full 3d render of the design with legs and pads">}} Success {{</ image>}}

Based on the feedback we will be doing a lot more 3D CAD design so I just might go with FreeCAD over SolveSpace. We'll see.
