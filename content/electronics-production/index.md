---
title: Electronics production
menu: project
weight: 7
---

Time to get my PCB design up and running. After the wild and admittedly exhausting week of CNC I'm ready to do something smaller.

### Milling the PCB

Exporting the pcb designs from KiCAD was fairly easy with some minor difficulties. I could not get CopperCam to open the designs on my computer very well but fortunately the milling machine computer had no issues.

First I imported the copper layer:

{{< image src="gerber1.jpg" alt="Picture of the copper part of the board">}} Copper cam does not like letters {{</ image>}}

Then the drill files:

{{< image src="gerber2.jpg" alt="Picture of the hole locations">}} Perfect alignment {{</ image>}}

If the holes and pads no not align properly, you can assign pads and holes as reference. Assigning two pads and two holes will align everything nice and smooth. If the holes are resized, you might need to readjust their size:

{{< image src="assignpad.jpg" alt="Right click menu in copper cam">}} Select as reference pad {{</ image>}}

{{< image src="assignhole.jpg" alt="Right click menu in copper cam">}} Select adjust to reference pad {{</ image>}}

You also have to assign all contours as cuts to get the holes and outside cut:

{{< image src="contour.jpg" alt="Selecting contour in copper cam">}} Inside for holes and outside for the, well, outside {{</ image>}}

{{< image src="cuts.jpg" alt="Toolpath in copper cam">}} Here are the assigned cuts {{</ image>}}

Here is the gerber files joint together in KiCad to see the whole design

{{< image src="gerbers.jpg" alt="Picture of the board design">}} The gerber files {{</ image>}}

Now lets get milling:

{{< image src="milling.jpg" alt="Roland small cnc machine milling">}} Hopefully I thightened the tool enough {{</ image>}}

No issues here. Phew! The board came out nice.

{{< image src="milled.jpg" alt="A copper pcb board">}} My first PCB. Think of the possibilites! {{</ image>}}

### Soldering the parts

Some parts were missing from the FabLab so I have to wait for them. Fortunately I could solder most of the parts already.

{{< image src="4pin.jpg" alt="R4 pin connector soldered to a board">}} 1st joint terrible, the others are fine {{</ image>}}

I decided to solder the first joint of the part to the ground plane so that even if the joint is terrible it will work and not cause issues. That joint then helped me solder the other joints better. After the header I decided to solder the LED, resistor and switch. Turns out the parts are super small and required a fair amount of dexterity from me to get in place. Reflow soldering was not an option as the solder paste had dried out.

{{< image src="how.jpg" alt="A small LED on a PCB">}} Some magnifying required {{</ image>}}

I managed to get all of the small parts soldered and some of the joints look real good. Great success! I tested continuity with a multimeter and the circuit works fine.

{{< image src="soldered.jpg" alt="PCB with most parts soldered">}} Now to add the XIAO board {{</ image>}}

Now to solder the headers. This turned out to be a bit of an issue as the headers are mounted in the holes but I need to solder  the pins to the pads on top of the board. Fortunately leaving some space between the headers and the board worked and soldered the headers without issue.

{{< image src="headers.jpg" alt="PCB with headers soldered in">}} Just plug it in and you are good to go {{</ image>}}

{{< image src="xiao.jpg" alt="PCB with the XIAO board plugged in">}} Fits perfectly! {{</ image>}}

Testing continuity again I found two pins that had connection to ground but the resistance was millions of ohms. After hours of scraping the board I managed to get the excess copper off and got continuity in its place.

Now to test. Here's my code:

```c
int neo1 = 26;
int neo2 = 27;
int led = 28;
int button_pullup = 29;
int fourpin1 = 2;
int fourpin2 = 3;
int fourpin3 = 4;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(button_pullup, INPUT_PULLUP);
  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);

}

void loop() {
  // put your main code here, to run repeatedly:
  if(digitalRead(button_pullup) == LOW){
    Serial.println("closed");
  }
  else{
    Serial.println("open");
  }

}
```

I had a bit of an issue though. The LED did not work and after some testing I realized the ground pin on the XIAO board and the ground plate on the pcb had an almost 2V drop and a fair bit of resistance. I resoldered the ground pin to the pcb and now everything works fine. The LED is really weak, perhaps my resistor is too strong but overall the board works.

{{< image src="led.jpg" alt="PCB with a powered on LED">}} Just a tiny amount of red {{</ image>}}
