---
title: Documentation website
menu: website
---
The first week! Yay! I was excited to finally enter the FabLab (or as I like to call it, the fabrication labrication). After years of working with clay I'm finally getting to other materials.

## Starting out

The first task was to build a simple documentation website. I had previous experience creating websites so the initial setup was quite a breeze. Setting up Git and Gitlab went  smoothly and without issue. I created the project, wrote some html and added two pictures. I noticed that the Bootstrap css framework was coming later on the course so I decided against any css for now.

{{< image src="FirstSite.jpg" alt="Picture of the first version of the site">}}The first version of the site {{</ image>}}

## Week 2 rolls up with a huge Hugo

Hugo is a static website generator. It is used to maintain sites with large amounts of content. I was really excited about this because while I had worked with React before, React seemed excessive for just content management. After some fiddling around I figured Hugo out and remade the page:

{{< image src="secondsite.jpg" alt="Picture of the second version of the site">}}The second version of the site {{</ image>}}

The second week's assignment required us to add a page for the final project, which I added with a gigantic picture. The size will be fixed later when css appears.

{{< image src="secondsite2.jpg" alt="Picture of the final project page">}} The final project page (rather ugly at the moment) {{</ image>}}

After that I added normalize.css for fixing inconsistencies between browsers and linked to bootstrap for some low effort visual flair.

{{< image src="secondsite3.jpg" alt="Picture of the site with bootstrap">}} Now with some bootstrap {{</ image>}}

Most of my second week was spent with extracurricular 3d-printing and helping out other students. I didn't want to waste time on work that would later be changed anyway.

## Week 3, pulling the site up by bootstraps and exciting image options

{{< blockquote author="W3Schools">}} Bootstrap is the most popular CSS Framework for developing responsive and mobile-first websites.{{</ blockquote>}}

Now we are talking. The website must be done next week and then we'll start fabricating stuff (not that it stopped me, I printed a bunch of stuff off hours with the resin printer). I started by adding bootstrap properly though just as a meta link for now. Later I might download the full bootstrap package to get the site working offline but this will do for now.

Now I know some bootstrap and the general css frameworks workflow so this should be a breeze right? Well, mostly. Basic bootstrap stuff I got just fine. Header, footer, navbar all that jazz. Then I ran into a bit of a problem: to add a css class to an html tag requires access to that tag. But Hugo generates images straight from markdown so the "img" tags will come without classes. Initially I assumed only images would be an issue so I disregarded Hugo shortcodes and wrote this script to make all images responsive:

``` html
<script>
  var list = document.querySelectorAll("img");
  list.forEach(img => { img.classList.add("img-fluid"); img.classList.add("shadow-sm")});
</script> 
```

It worked pretty nicely as you can see:

{{< image src="bootstrapishere.jpg" alt="Picture of the site with bootstrap used properly">}} Now with a ton of bootstrap {{</ image>}}

However then I realized blockquotes need classes as well for bootstrap. This means I'll have to write a bunch of custom shortcodes anyway so the img tag changed as well. (I finally decided to skim through the course tutorial video.) Now with shortcodes, blockquotes and images the page is getting rather nice.

{{< image src="bootstrapped.jpg" alt="Picture of the site with more bootstrap stuff">}} Footer is here! {{</ image>}}

The standard Bootstrap layot is quite close to the design I use for my current CV so I decided to download bootstrap into the static folder and linked to Google Fonts Montserrat for some customization. I will change the color and perhaps the size of navbar and footer to match my CV.

{{< image src="cv.jpg" alt="Picture of my CV">}} CV made with Figma {{</ image>}}

Next up I added an icon with "DF" for Digital Fabrication.

{{< image src="icon1.jpg" alt="Picture of my website icon">}} "DF" for Digital Fabrication {{</ image>}}

The square icon had a bit of an issue: It would fill out the whole page and be really tall. I decided to squeeze down the pictures by overriding bootstrap and adding `style="width: 100%; max-width: 700px;"`. Now pictures will be wide enough on mobile but slimmer on desktop. I added the same functionality to the main page container that now limits the width of the page to `1000px` on any device. Mobile will naturally enjoy pagewide text. It took a ton of work to get images to scale on mobile and limit their growth on desktop. This shortcode for blockquote did the trick:

``` html
<div class="row m-3 justify-content-center ">
    <div style="width: 100%; max-width: 700px;">
        <div class="row justify-content-center">
            <img class="img-fluid mb-3 shadow-sm"
                src="{{ .Get `src`}}"
                alt="{{ .Get `alt`}}"
            >
        </div>
        <p style="text-align: center;">{{ .Inner }}</p>

    </div>
</div>
```

Next up, the frontpage. I wanted to add a grid selection that links to all of my projects. The bootstrap grid works fairly well for this. I decided to write the whole page manually because I might need a ton of customization ability.

{{< image src="grid.jpg" alt="Picture of the frontpage grid">}} Grid that links to pages on the site{{</ image>}}

With the frontpage I needed some content so I decided to complete the assignment and write git and media workflow pages. No issues there. I completed the about section on the site with some links to my other websites.

The site is now ready. There are no screenshots for the final site because you can just take a look at the site yourself. I hope you find this documentation and my projects useful!
