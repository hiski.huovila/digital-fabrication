---
title: Learning CAD
menu: project
weight: 1
---

The website is now set up so it is time for Computer Aided Design aka CAD. This is exciting I've never really used any CAD before. Inkscape and GIMP are familiar programs so I don't expect too much trouble but new is always new. This will be very useful both for my fabrication needs but also my career in general.

### Choosing a CAD

There are at least five different CAD software recommended by the course: OpenSCAD, FreeCAD, Blender, Fusion360 and SolveSpace. To make choosing easier I created this table to compare them

| Feature ------ |  OpenSCAD ------ |  FreeCAD ------ |  Blender ------ |  Fusion360 ------ |  SolveSpace |
|---------|----------|---------|---------|-----------|------------|
|Price|free|free|free|free for uni|free|
|Interface|standard|clunky|nice|nice|nice|
|Features|many|many|way too many| many|probably most limited|
|Famliarity|none|none|very little|UX pretty good|I use Inkscape|
|Biggest flaw|typing|bad UI|too much|eventual price|least features|

Based on this comparison I've chosen to learn SolveSpace. OpenSCAD can probably do an insane amount of things but I dislike reading documentation while drawing. FreeCAD is second on my list but SolveSpace has much nicer UI design. Blender would probably be really good to learn but I think 3D modeling software such as Blender is a bit much for design drawings. I decided to skip Fusion360 because I will not purchase it later and I want to be able to use the software I learn even after any university licenses expire.

### Lets take a look

Installation of SolveSpace was easier than any other software yet. I just downloaded `solvespace.exe` from [their website](https://solvespace.com/download.pl). First opening UI is clean and nice, all tools are in a nice sidebar and everything can be found at a glance. A good fit for me as I tend to learn software by trying out different tools. I'm also fairly good at finding any tools I need based on context clues so these lovely icons should do the trick.

{{< image src="niceui.jpg" alt="Picture of SolveSpace user interface">}} The SolveSpace toolbox {{</ image>}}

### What to draw

I've decided to design a stand for a mini sized projector, which could then project towards a table using a mirror. Assuming the projector is the size of the [Meer mini projector](https://projectoreviews.com/meer-yg300-projector/) it would be around 9,6cm long and 5,5cm tall. Its width should not affect the design much. Setting the stand on a table it should be a maximum of 30cm tall so that the mirror always sits comfortably below eye level.

{{< image src="sketch.jpg" alt="Marker and digital sketch of the project">}} Some sketches {{</ image>}}

I started drawing with SolveSpace and using parallels, equal lengths and constraints. Initially I intended the stand to have a sort of X-shape to minimize the footprint but I decided to switch to a +-shape instead because it will make mounting the mirror a lot easier.

{{< image src="standcad1.jpg" alt="Picture of the projector stand CAD file">}} First design after some work {{</ image>}}

Then I realized the hole for the mirror is not in a position that will work so I redesigned the hole to be exactly at the center of the projector beam. This lead to this slot design that will allow me to adjust the distance from the mirror at least until I figure out a good distance.

{{< image src="withadjustableslot.jpg" alt="Picture of the projector stand CAD file with an adjustable slot">}} Second design {{</ image>}}

However that CAD seems a little hard to read. I decided to clean up some of the markings and make use of the parallel lines tool. It may not seem more clean from a distance but considering everything works in 45 degree angles makes it easy to understand up close.

{{< image src="muchcleaner.jpg" alt="Picture of the projector stand CAD file with cleaned up constraints">}} Cleaned up constraints {{</ image>}}

Now for some 3D magic. I extruded the model to 3D so I can create slots for cuts so that I can slot parts to form the +-shape. I realized I might not need to make another part as the remaining cutouts from the edges can be used to form the leg and the projector support. It took some work to get the constraints right (as I learned you should have the initial length set up at the earliest possible point and then cosntraint stuff to it later). Here's a picture of the model now:

{{< image src="3dmodel.jpg" alt="3D model of the project in SolveSpace">}} 3D extrusion {{</ image>}}

I exported the model as a `.obj` file. Turns out the whole thing comes out so my smart plywood saving cuts didn't really make the cut. Fortunately I can only export the first extrusion without the extra parts and use that model. I drew this quick sketch in Paint3D for the final product (it's really bad but it's quick and comes preinstalled on Windows):

{{< image src="render.jpg" alt="3D render of the project in Paint3D">}} Paint3D is not good software but this came out nice {{</ image>}}

### SolveSpace vs FreeCAD

I decided to try out SolveSpace's closest competitor: FreeCAD. Initally I decided to use SolveSpace due to its simplicity but now that I'm familiar with the basic functions it's time to see if FreeCAD does them better. I decided to redraw the same parts as my project for the next week of computer controlled cutting. See [Computer controlled cutting]({{< ref "/computer-controlled-cutting" >}}) for the 3d renders.
