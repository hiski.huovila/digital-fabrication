---
title: Electronics design
menu: project
weight: 5
---

Electronics design, huh? The exciting stuff just keeps coming. I've never designed electronics or really done anything with eletronics for that matter. I have some experience with Raspberry PI's and the XIAO board from two weeks ago. I know some theory about circuit design due to YouTube randomly recommending me stuff and from my high school physics classes.

### Measuring the operating voltage

Time to measure the operating voltage! The initial question is wether the XIAO RP2040 uses AC or DC current. After some digging I think it uses DC. I measured the voltage between the pin D0 and GND, so the first pin and ground. That gave me the following reading of 3.336 volts. Unsurprisingly that is indeed the operating current of the RP2040.

{{< image src="voltage.jpg" alt="Measuring the voltage of the XIAO RP2040">}} 3.336 volts! As expected. {{</ image>}}

I also managed to figure out that you should not connect the 3.3V pin and the GND directly as this will short circuit the board and force it to reboot. Whoops.

### KiCAD and board design

Installation went fairly easily, standard administrator stuff had to be dealt with but KiCAD did everything smoothly. First look at the UI I really dig the slightly larger and rounded icon style. Compared to FreeCAD I feel more professional working with this. Or at least a little more artsy than an engineer. A designer if you will.

Initially I started sketching and drew this initial part layout

{{< image src="firstsketch.jpg" alt="Components laid out in KiCAD">}} It's a start {{</ image>}}

Now I began adding connections. I was a bit confused by the 4-pin header as I don't know where I want to add the `clk` (supposedly meaning clock) pin or any other pin except `gnd` which I could identify as ground.

Let's try out this layout, figure out the workflow and ask questions later.

{{< image src="secondsketch.jpg" alt="Components laid out in KiCAD with connections">}} Now for the pcb {{</ image>}}

Then comes the pcb version of the layout:

{{< image src="pcb1.jpg" alt="The design on a pcb in KiCAD">}} Looking good {{</ image>}}

I also made a 3D render of the board using KiCAD:

{{< image src="3drender.jpg" alt="3d render of the board in KiCAD">}} I really like the 3d render feature {{</ image>}}

After a mostly succesfull board design it was time to add a NeoPixel. Unfortunately the fab library did not have a footprint for the NeoPixel (or I missed it somehow) so I had to improvise a little and use a footprint from the KiCAD pre-installed libraries. I also added a resistor to the LED to get it working properly. The LED is inteded to use about 2V of voltage and 20mA current. This means the resistor needs to use up 1.3v at 20mA current. Calculating with the formula R=U/I we will get R = 1.3/20*1000 = 65ohm. It's a little more than the 49.9ohm resitor we have but the LED can take up to 2.4V so it should be fine. If we were to run the LED with 2.4V the resistor needs to be only 30ohm, so this works out just fine.

{{< image src="withneo.jpg" alt="Components laid out in KiCAD with the NeoPixel added">}} This is getting complicated {{</ image>}}

And the same as a pcb:

{{< image src="pcbneo.jpg" alt="The board design in the pcb editor">}} Cleaning up the lines is so satisfying {{</ image>}}

Then some copper as a global ground.

{{< image src="pcbcopper.jpg" alt="The same pcb with copper filling most of the space">}} Some decorative text as well {{</ image>}}

And finally a render of the final board:

{{< image src="final3d.jpg" alt="3d render of the final board design">}} It looks so good in the render {{</ image>}}
