---
title: Media workflow
menu: website
---

Pictures and videos need some work to make them accessible. I talk about image scaling challenges in the website documentation. Here I will talk about optimization.

#### ImageMagick

To compress images I use [ImageMagick](https://imagemagick.org/). It is a powerful tool to mass edit pictures. My computer saves screenshots as `.png` files, which are nice but have too much data with them for the purposes of this website. To shrink the files down to a more digestible size I run

```bash
magick mogrify -format jpg *.png 
```

which changes all png files in the directory to `.jpg` files. Jpegs are much smaller than pngs though this conversion is lossy so it is not reversible and information will be lost.

#### Ffmpeg

[Ffmpeg](https://ffmpeg.org) is a tool to edit and play video files. You can install it independently or in my case it was an option when installing ImageMagick.

Large videos take a ton of space and need a good internet connection to download. With Ffmpeg I can run

```bash
ffmpeg -y -i <inputname>.mp4 -c:v libx264 -crf 23 -profile:v high -pix_fmt yuv420p -color_primaries 1 -color_trc 1 -colorspace 1 -movflags +faststart -an <outputname>.mp4
```

to compress videos by around 40%. All videos I record should be in fullHD 1080p so I don't inted to rescale them. If the above command still leaves videos too large I will compress them to 720p.
