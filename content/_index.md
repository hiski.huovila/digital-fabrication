---
title: Hiski Huovila - Fab Academy
---

Welcome to my documentation site! Here you will find all of my projects and documentation on the build processes from the Digital Fabrication minor in Aalto University. The course follows the global Fab Academy schedule. Below you'll find links to all of my projects.
