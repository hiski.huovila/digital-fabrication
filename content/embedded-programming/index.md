---
title: Embedded Programming
menu: project
weight: 3
---

Embedded programming with the Seeed XIAO RP2040 chip. Excellent. I have some experience with Raspberry Pi's but not with Arduino so this will be just the perfect difficulty for me. In this assignment I had to build a device using the RP2040 neopixel LED and a button (and some wires).

#### The soldering

First things first let's solder the pins to the board. I learned that this was a bad idea and I should have tested the board and done some programming before soldering so that I know how the board should work. Oh well.

It had been 10 years since I last soldered but I had watched some tutorials so this should be fine, right? Well, here is my first solder:

{{< image src="terriblesolder.jpg" alt="Some badly soldered connections">}} Oh wow this is tough. Fortunately these work. {{</ image>}}

Turns out soldering is not as easy as it seems. But after some trying I managed to get going:

{{< image src="startingout.jpg" alt="Continuation of soldering">}} This is getting better {{</ image>}}

I had some issues and wanted to remove some solder but the soldering braid didn't really work as I thought it would so I left most of the solder as is. Over time I started to figure soldering out and managed to get the other side of the chip pretty good:

{{< image src="bettersolder.jpg" alt="A better solder than previously">}} Much better {{</ image>}}

Due to my inexperienced soldering I whipped out a multimeter and tested continuity between the pins. All solders were fine, thankfully.

{{< image src="continuitytest.jpg" alt="Testing continuity with a multimeter">}} Everything is in order {{</ image>}}

#### The programming

Installation of Arduino IDE and the libraries went fine (later on you'll find out it didn't) and I got the thing connected and the LED blinking with the blink example. Here's the code:

``` c
// the setup function runs once when you press reset or power the board
void setup() 
{
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}
 
// the loop function runs over and over again forever
void loop() 
{
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(5000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(2000);                       // wait for a second
}
```

{{< video src="blinkingled.mp4" alt="A blinking led light in the chip">}} Here we go {{</ video>}}

Then I got the Neopixel rgb-LED working and blinking with colors:

```c
#include <Adafruit_NeoPixel.h>
 
int Power = 11;
int PIN  = 12;
#define NUMPIXELS 1
 
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);
 
void setup() 
{
  pixels.begin();
  pinMode(Power,OUTPUT);
  digitalWrite(Power, HIGH);
 
}
 
void loop() { 
  pixels.clear();
  pixels.setPixelColor(0, pixels.Color(15, 25, 205));
  delay(400);
  pixels.show();
  pixels.clear();
  pixels.setPixelColor(0, pixels.Color(103, 25, 205));
  delay(400);
  pixels.show();
  pixels.clear();
  pixels.setPixelColor(0, pixels.Color(233, 242, 205));
  delay(400);
  pixels.show();
  pixels.clear();
  pixels.setPixelColor(0, pixels.Color(233, 23, 23));
  delay(400);
  pixels.show();
  pixels.clear();
  pixels.setPixelColor(0, pixels.Color(12, 66, 101));
  delay(400);
  pixels.show();
  delay(500); 
}
```

{{< video src="blinkingpixel.mp4" alt="A blinking larger LED in the chip">}} Nice but really bright {{</ video>}}

Then began the issues. I tried to get the button working and eventually found out how to connect the button properly but then I had another issue. For some reason touching the chip with my hand made the color change (color change was the intention but from the button and not my hand).

{{< video src="what.mp4" alt="The LED changes color when I touch the soldered pin">}} What {{</ video>}}

Turns out if you use INPUT and not INPUT_PULLUP in the pin you connect the button to without a resistor you will get a random read from the pin at random times. So I got it fixed and the button in place and this is how it looks now:

```c
#include <Adafruit_NeoPixel.h>

#define NUMPIXELS 1
 
int power = 11;
int PIN  = 12;
int powerread = 26;

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  pinMode(power, OUTPUT);
  digitalWrite(power,HIGH);
  pinMode(powerread, INPUT_PULLUP);
  pixels.begin();
  pixels.setBrightness(10);
  pixels.clear();
}

void loop() {
  //button is open
  if(digitalRead(powerread) == HIGH) {
    
      pixels.clear();
      pixels.setPixelColor(0, pixels.Color(205, 25, 15));
      pixels.show();
  }
  //button is closed
  else {
      pixels.clear();
      pixels.setPixelColor(0, pixels.Color(15, 25, 205));
      pixels.show();
  }
  
}
```

{{< video src="ledcontrol.mp4" alt="The LED is turned on and off using the button">}} Pressing the button turns the LED on and off {{</ image>}}

Then I wanted to send color data to the LED through the serial port. Initially everything worked fairly easily until I wanted to also change the builtin led's and not just the Neopixel LED. After some frustration I figured out I had installed an old and deprecated version of the RP2040 library which prevented me from accessing all of the features. With that sorted I managed to get the device to work properly in this manner:

You type in a value between 0 and 360 as degrees in the color wheel. The Neopixel is changed to that color and the builting rgb-LED shows either red, green or blue depending on which is the closest to your color. Any values outside 0 and 360 are counted as a 0 with an error message. The button switches the big LED on and off.

#### The serial

The serial port essentially sends data through the USB port to the RP2040. By using `Serial.begin(9600)` you can start a serial connection with the computer. The Arduino IDE provides a field to send and receive data through the serial port. Using `if (Serial.available() > 0)` in the `loop()` we can access serial data whenever the port makes a connection and not read empty lines. Using `Serial.readString()` I read a value from the serial that I use to edit the hue value of the leds. Then, with `Serial.println(hue)` I send the received hue back to the computer to confirm that the read was succesful. If the hue is incorrect I send an error message.

#### The final results

{{< video src="finalized.mp4" alt="A video of the final product as described above">}} Finally, what a relief {{</ video>}}

Here's the code:

```c
#include <Adafruit_NeoPixel.h>

#define NUMPIXELS 1
int power = 11;
int out = 0;
int PIN  = 12;
int powerread = 26;
float hue = 0;
bool state = true;

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
  pinMode(power, OUTPUT);
  digitalWrite(power,HIGH);
  pinMode(out,OUTPUT);
  digitalWrite(out, HIGH);
  pinMode(powerread, INPUT_PULLUP);
  pixels.begin();
  pixels.setBrightness(10);
  pixels.clear();

  pinMode(PIN_LED_G,OUTPUT);
  pinMode(PIN_LED_R,OUTPUT);
  pinMode(PIN_LED_B,OUTPUT);
  
}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    String str = Serial.readString();
    hue = str.toFloat();
    Serial.println("Changing to: ");
    Serial.println(hue);
    if(hue < 0 || hue > 360) {
    Serial.println("Set hue between 0 and 360, hue set to 0");
    hue = 0;
    }
  }

  digitalWrite(PIN_LED_R, HIGH);
  digitalWrite(PIN_LED_G,HIGH);
  digitalWrite(PIN_LED_B,HIGH);
  
  if( 60 > hue && hue >= 0){
    digitalWrite(PIN_LED_R,LOW);
  } else if( 180 > hue && hue >= 60 ){
    digitalWrite(PIN_LED_G,LOW);
  }
  else if( 300 > hue && hue >= 180 ){
    digitalWrite(PIN_LED_B,LOW);
  }
  else if( 360 >= hue && hue >= 300){
    digitalWrite(PIN_LED_R,LOW);
  }

  //button is open
  if(digitalRead(powerread) == HIGH) {
      if(state){
        pixels.clear();
        float to16bit = (hue / 360) * 65535;
        pixels.setPixelColor(0,pixels.ColorHSV(to16bit,255,255));
        pixels.show();
      } else {
        pixels.clear();
        pixels.show();
      }
  }
  //button is closed
  else {
    state = !state;
      delay(500);
    }
}
```
