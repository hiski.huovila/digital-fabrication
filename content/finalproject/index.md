---
Title: Final Project
---

#### Preparations

 I got an idea to improve my tabletop rpg experience by adding lights to my games. This poses an issue though: How to power LED lights in a way that allows me to build multiple different scenarios and add lights in multiple ways. The solution: wireless power. Using the same technology as found in wireless chargers I decided to make a game board (traditionally with a 1 inch grid) that houses an inductive coil inside and lets me power coils with LED's on top of it. To understand the physics behind this, read [Wikipedia](https://en.wikipedia.org/wiki/Wireless_power_transfer)

This project itself is not super unique as there are plenty of examples online where people have built wirelessly powered LED's. Many of them are quite specific use cases for fun but I'm interested in making a working product. Adafruit sells [this product](https://www.adafruit.com/product/5140) that is pretty much the component I want to build. However, as I only need the power to be transmitted a short distance, I might be able to make the coil bigger and thus serve a larger area. Commercial wireless LED's such as the Adafruit one are general purpose components so they might not fit my requirements perfectly. I will follow [this](https://youtu.be/jdc_0r5pJPc) video by atomic14 where he essentially replicates the Adafruit circuit. I will design the circuit board myself and add a wooden laser cut grid on top of the coil. I will also design and 3d-print some parts that use the LED's.

My week 10 documentation on input devices holds a large amount of the research that was done in  preparation of this project.

{{< image src="circuit.jpg" alt="Circuit diagram">}} Here's the circuit diagram from the video {{</ image>}}

{{< image src="schematic1.jpg" alt="Kicad schematic">}} And here's the schematic {{</ image>}}

Following this video I started designing a pcb. I fabricated the circuit but burned some components while testing.

{{< image src="1stpcb.jpg" alt="Pcb with components">}} Did not work {{</ image>}}

I started out using this code

```c
#include <Arduino.h>

const int ledPin = D0; // GPIO pin connected to the LED

void setup()
{
  pinMode(ledPin, OUTPUT);
  analogWriteResolution(8); // Set the PWM resolution to 8 bits (0-255)
  analogWriteFreq(228000); // Set the PWM frequency to 228 kHz
  analogWrite(ledPin, 102); // Set the PWM duty cycle (0-255)

}

void loop(){}
```

but it only worked with low frequencies.

I also tried this PWM library specifically for the RP2040. It worked but also only up to 2kHz, and I need to get up to 200kHz.

```c

#define _PWM_LOGLEVEL_        3
#include <RP2040_PWM.h>

//creates pwm instance
RP2040_PWM* PWM_Instance;

float frequency;
float dutyCycle;

#define pinToUse      D0

void setup()
{
  //assigns pin 25 (built in LED), with frequency of 20 KHz and a duty cycle of 0%
  PWM_Instance = new RP2040_PWM(pinToUse, 1500, 20);
  
  if (PWM_Instance)
{
  PWM_Instance->setPWM();
}

}

void loop()
{}
```

I was unsure about my self made coil, so we ordered some 2.2 mH inductors and also an adafruit wireless LED kit.

{{< image src="adafruit.jpg" alt="Working wireless LED's">}} The Adafruit coil works with the Adafruit LED's {{</ image>}}

{{< image src="adafruit2.jpg" alt="Working wireless LED">}} And with my LED as well {{</ image>}}

Measuring the voltage and current used by the coil it turns out I don't need many amps of power to run it. This means I should be able to lower voltage or add resistors and run with less current. Then I should be able to not burn any more components.

{{< image src="measuring.jpg" alt="Square-ish waveform">}} It works on low frequencies {{</ image>}}

I figured out that the mossfet is probably not turning on at high frequencies. I could go to about 3kHz before the circuit no longer had a stable frequency. From the datasheet I figured out that the mossfet's transition frequency is 250 MHz which means the approximately 200kHz frequency I need should be fine. This means the issue is either with the rp2040 chip or between the mossfet and the board. I assume the issue is caused by the fact that the xiao board only gives 3.3v of power and the mossfet is intended to use 5v. With increasing frequency the mossfet does not have enough power to react rapidly. I decided to use a 555 timer instead of the xiao board to run the coil.

#### 555 time

A 555 timer is a device that can be configured to send a single pulse or a constant square wave. Depending on the input resistors and capacitor, many different waves can be generated.

{{< image src="schematic2.jpg" alt="Kicad schematic again">}} A new design {{</ image>}}

{{< image src="coppercam.jpg" alt="PCB in copper cam">}} Lets mill {{</ image>}}

{{< image src="engraved.jpg" alt="PCB made from copper">}} Now we solder {{</ image>}}

{{< image src="built.jpg" alt="Wireless charging device">}} Here it is{{</ image>}}

{{< image src="555calculation.jpg" alt="Digikey 555 calculation">}} Here are my component values for the 555{{</ image>}}

After a large amount of testing I figured out that the resistor is no longer needed as the 555 helps with current regulation. However, I could only measure a 50Hz sinewave from the circuit.

{{< image src="sinewave.jpg" alt="sinewave in an oscilloscope">}} Odd {{</ image>}}

I ran a simulation on the circuit and it is indeed not working.

{{< image src="simulation.jpg" alt="Simulation of the circuit">}} Its not working {{</ image>}}

So I tweaked the simulation to run on 1kHz and it works in the simulation

{{< image src="simulation2.jpg" alt="Simulation of the circuit">}} Wild isn't it {{</ image>}}

This proved to me that the 555 is working and soldered properly. I did extensive testing to figure out that there are no bad connections causing issues.

{{< image src="squaresine.jpg" alt="Squarewave inside a sinewave">}} What {{</ image>}}

So there is indeed a squarewave inside the sinewave. This means the 555 is working at least somewhat. I did some research on 555's and figured out that the components I use might just be too small of a resistance or too much of capacitance.

{{< image src="simulation3.jpg" alt="Simulation of the circuit">}} This should do it {{</ image>}}

{{< image src="working.jpg" alt="Wireless power working">}} Oh my god it is working {{</ image>}}

{{< image src="workingwave.jpg" alt="Oscilloscope wave">}} And the working waveform as well {{</ image>}}

I was right about the components. The Adafruit LED's are not turnign on with this one though. I assume it is because the Adafruit LED's are tuned to the 220kHz frequency. Now I will try to tune my circuit to that frequency as well.

{{< image src="simulation4.jpg" alt="Simulation of the circuit">}} Lets try this now {{</ image>}}

Here is a [link](http://www.falstad.com/circuit/circuitjs.html?ctz=CQAgjCCsCmC0Cc4AsA6ATAZkgNgxg7JAAz5KRqQAcISaUNGUcYYAUADY2VHhrVLcQGbNR5EURSUXgzZc+YnFE0aIknzZIYJMrRgMmbCCKswmkGh3hsPHDzM8jqecSlFXbAE4WrYPj-t4Oh4keBR4Vm9LQLpoixxjGhlw1gA3ZCQLfDptTNVRRKtIRLEUSFYAdwDealya40qM+rr8hqrhWv8MNXqTKpbspo62oWEhEVGjAzFWADMaM0mFqYmDcDLjCTRGpEWVTOH9oR3FvCc9yxGkNeGBHmG+8c7+QT9RRuG3p-Agkbi+aj-BKPf6UQFWaYjbqZL5xL4giETOoPRpxYZnb6PDG3SSY1gAY2qkLRExCKHwbkpbjAcEQGBQfiImgwfmwOjAkEgmSURDYhMgGgsYKggoEiRgCBAsEgDMg8DwlDA+DMLEVEB5bCq5GeXHs-ke2u+dzx3mxE12RiO9ipBJA8EtCXtvU21IUbpk1GlLt5jSdXzq8MidpW1CdFCM9lpvpDQ1JOydToxghMAHtEmt7iJCOBSu5CumSunWGmWEJgk9s8UeZJ4BocBRwBZwBAMMXkGXEmZ8PAtPRxLEm9pjhgCgAxdUuiBgCQQWAQACSADsACYAV3xABcAIaL-HQVhAA
) to the simulation as well.

I built this test LED that should work with 220kHz frequency.

{{< image src="testled.jpg" alt="Powered on LED">}} And it is working {{</ image>}}

I made a new PCB with the new components

{{< image src="underneath.jpg" alt="Powered on LED on a mat">}} It works through materials {{</ image>}}

Next up I multiplied the number of coils and to my surprise it still works.

{{< image src="morecoils.jpg" alt="Four coils lighting up LED's">}} Amazing {{</ image>}}

Now we tape the coils underneath this laser cut grid

{{< image src="taping.jpg" alt="Four coils and a wooden plate">}} Almost done {{</ image>}}

### Presentation

{{< image src="presentation.png" alt=">Final project slide">}} In short {{</ image>}}

{{< video src="presentation.mp4" alt="Final project video">}} Wireless Powered Combat Grid {{</ video>}}

Keep in mind that the above video is a demonstration and does not accurately reflect the capabilities of the product. In general use the coils must be placed onto the grid for proper power transfer and should be incorporated to the bottoms of any miniatures used.

### Bill of Materials

#### The grid

-----------------------------------------------------------------------------------
| Item                  | Price           |Qty        |  Digi-Key Number           |
|-----------------------|-----------------|-----------|----------------------------|
| Capacitor 100nF       | 0,33 €          |  1        |399-C1206C104KARAC7800CT-ND |
| Capacitor 4.7nF       |0,33 € (estimate)|  1        |N/A                         |
| Capacitor 10nF        |0,33 € (estimate)|  1        |N/A                         |
| MOSFET N-CH 30V 1.7A  | 0,25 €          |  1        |DTC143EUA-TPMSCT-ND         |
| 555 timer 8SOIC       | 1,02 €          |  1        |LMC555CMX/NOPBCT-ND         |
| Resistor 0ohm         | 0,09 €          |  1        |311-0.0ERCT-ND              |
| Resistor 49.9ohm      | 0,09 €          |  1        |311-49.9FRCT-ND             |
| Resistor 499ohm       | 0,09 €          |  1        |311-499FRCT-ND              |
| Resistor 1Mohm        | 0,09 €          |  1        |311-1.00MFRCT-ND            |
| Magnet wire 28AWG     | 0,305€          |350cm      |1175-1713-ND                |
| Plywood (laser) 2mm   | 2,10€ (inc VAT) |25,6x25,6cm|N/A                         |
| FR2 single sided      | 0,095 €         |28x28mm    |N/A                         |
-----------------------------------------------------------------------------------
Total price 5,12€

Prices do not include VAT unless otherwice specified. Manufacturing materials such as solder or tape are not counted.

#### Wireless LED's

If you want to build a wireless LED to use with the grid

-----------------------------------------------------------------------------------
| Item                  | Price              |Qty   |  Digi-Key Number  |
|-----------------------|--------------------|------|-------------------|
| Inductor 2.2mH        | 0,51 €             | 1    |RLB1014-222KL-ND   |
| LED white             | 0,17 €             | 1    |160-1167-1-ND (N/A)|
| Capacitor 220 pF      | 0,33 € (estimate)  | 1    |N/A                |
-----------------------------------------------------------------------------------
Total price 1,01 €

Prices do not include VAT unless otherwice specified. Manufacturing materials such as solder or tape are not counted.

{{< image src="ledschematic.jpg" alt="Schematic of led">}} Here's a schematic for the LED {{</ image>}}

#### Casing

The electronics need some kind of shield to protect the user from them and them from damage. I designed this simple 3d printed casing.

{{< image src="casewires.jpg" alt="3d printed case">}} The wires exit nicely {{</ image>}}

{{< image src="caseusb.jpg" alt="3d printed case">}} A nice snug usb-c power input{{</ image>}}

{{< image src="caseopen.jpg" alt="3d printed case open">}} The electronics fit inside just right {{</ image>}}

### Future

In the future I also need to add some kind of regulator or fuse so that the device will not burn if there is a current spike in long term use.

After the project becomes safe to use I will be using it in my rpg games. Other future developments include 3d printing special miniatures with coils inside of them.
