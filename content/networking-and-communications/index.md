---
title: Networking and communications
menu: project
weight: 12
---

This week we were given a XIAO ESP32C3 board. This device is similar to the previously used XIAO RP2040 but instead of an adressable Neopixel LED it has a WiFi antenna, which we will use to communicate with it. I started with soldering headers to the board and to my surprise noticed I've become quite good at soldering. All joints are clean, use just a tiny amount of solder and I didn't need to resolder anything at all.

{{< image src="goodsolder.jpg" alt="Headers soldered to XIAO ESP32C3">}} Way better than my previous headers {{</ image>}}

I wanted to reuse my old pcb from the output devices week. Initially I made these modifications to my previous code to get the adressable LED on the pcb to work:

```c
#include <Adafruit_NeoPixel.h>
#include <TM1637.h>

#define NUMPIXELS 1

int neoout = D0;
int neoin = D1;
int led = D2;
int button_pullup = D3;

float hue = 0;
bool state = true;
bool wasopen = true;

Adafruit_NeoPixel bigpixel(NUMPIXELS, neoin, NEO_GRB + NEO_KHZ800);

const int CLK = D8;
const int DIO = D9;
TM1637 tm1637(CLK, DIO);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps

  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);

  pinMode(button_pullup, INPUT_PULLUP);
  bigpixel.begin();
  bigpixel.setBrightness(10);
  bigpixel.clear();

  pinMode(CLK,OUTPUT);
  pinMode(DIO,OUTPUT);

  tm1637.init();
  tm1637.set(BRIGHTEST);
  Digitdisplay();
}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    String str = Serial.readString();
    hue = str.toFloat();
    Serial.println("Changing to: ");
    Serial.println(hue);
    if(hue < 0 || hue > 360) {
      Serial.println("Set hue between 0 and 360, hue set to 0");
      hue = 0;
    }
    Digitdisplay();
  }
  
  //button is open
  if(digitalRead(button_pullup) == HIGH) {
      if(!wasopen) {
        wasopen = true;
      }
      if(state){
        bigpixel.clear();
        float to16bit = (hue / 360.0) * 65535.0;
        bigpixel.setPixelColor(0,bigpixel.ColorHSV(to16bit,255,255));
        bigpixel.show();
      }
      else {
        bigpixel.clear();
        bigpixel.show();
      }
  }
  //button is closed
  else {
    if(wasopen){
      state = !state;
      wasopen = false;
    }
    }
}

void Digitdisplay() {
    if(hue == 0){
      tm1637.displayStr("   0");
    }
    else {
    tm1637.displayNum(hue);
    }
    delay(50);
}
```

#### Going wireless

Now that the board was working I used `AsyncTCP`, `ESPAsyncTCP` and `ESPAsyncWebSrv` libraries to create a REST API endpoint to control the board and read its state. Here is my code:

```c

#include <Adafruit_NeoPixel.h>
#include <TM1637.h>
#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebSrv.h>

#define NUMPIXELS 1

// Some variables we will need along the way
const char* ssid     = "Fablab";
const char* password = "********"; 
const char* PARAM_MESSAGE = "message"; 
int webServerPort = 80;

// Setting up our webserver
AsyncWebServer server(webServerPort);

// This function will be called when human will try to access undefined endpoint
void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

//hardware pins
int neoout = D0;
int neoin = D1;
int led = D2;
int button_pullup = D3;

//variables
float hue = 0;
bool state = true;
bool wasopen = true;

Adafruit_NeoPixel bigpixel(NUMPIXELS, neoin, NEO_GRB + NEO_KHZ800);

//display setup
const int CLK = D8;
const int DIO = D9;
TM1637 tm1637(CLK, DIO);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps

 delay(10);

  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");

  // We want to know the IP address so we can send commands from our computer to the device
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Greet human when it tries to access the root / endpoint.
  // This is a good place to send some documentation about other calls available if you wish.
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", "Hello, world");
  });

  // Usage IP_ADDRESS/led?state=1 where /led is our endpoint and ?state=on is a variable definition.
  // You can also chain variables like this: /led?sate=off&color=blue
  server.on("/led", HTTP_GET, [](AsyncWebServerRequest *request){
    bool ledstate; // LED state 
    if (request->hasParam("ledstate")) {
      // The incoming params are Strings
      String param = request->getParam("ledstate")->value();
      // .. so we have to interpret or cast them
      ledstate = ((param == "on") ? HIGH : LOW); // Look up Terary Operator (e.g. https://www.programiz.com/cpp-programming/ternary-operator)
    } else {
      ledstate = LOW;
    }

    // Send back message to human
    String response = "Turning LED ";
    response += ledstate ? "on" : "off"; 
    request->send(200, "text/plain", response);

    // Operate LED
    digitalWrite(led, ledstate);
  });

  server.on("/neopixel", HTTP_GET, [](AsyncWebServerRequest *request){
    if (request->hasParam("hue")) {
        // The incoming params are Strings
        String param = request->getParam("hue")->value();
        // .. so we have to interpret or cast them

        if(param == "get") {
            request->send(200, "text/plain", String(hue));
        } else {
        hue = param.toFloat();

        if(hue < 0 || hue > 360) {
            hue = 0;
        }

        Digitdisplay();
        // Send back message to human
        String response = "Changing to: ";
        response += hue; 
        request->send(200, "text/plain", response);
        }
    }

    if (request->hasParam("brightness")) {
      // The incoming params are Strings
      String param = request->getParam("brightness")->value();
      // .. so we have to interpret or cast them
      if(param == "get") {
        request->send(200, "text/plain", String(bigpixel.getBrightness()) );
      } else {
      float brightness = param.toFloat();
      bigpixel.setBrightness(brightness);
      request->send(200, "text/plain", "Set brightness to: " + String(brightness));
      }
    }
  });


  // If human tries endpoint no exist, exec this function
  server.onNotFound(notFound);

  Serial.print("Starting web server on port ");
  Serial.println(webServerPort);
  server.begin();

 // hardware stuff
  pinMode(led,OUTPUT);
  digitalWrite(led,HIGH);

  pinMode(button_pullup, INPUT_PULLUP);
  bigpixel.begin();
  bigpixel.setBrightness(10);
  bigpixel.clear();

  pinMode(CLK,OUTPUT);
  pinMode(DIO,OUTPUT);

  tm1637.init();
  tm1637.set(BRIGHTEST);
  Digitdisplay();
}

void loop() {

  if (Serial.available() > 0) {
    // read the incoming byte:
    String str = Serial.readString();
    hue = str.toFloat();
    Serial.println("Changing to: ");
    Serial.println(hue);
    if(hue < 0 || hue > 360) {
      Serial.println("Set hue between 0 and 360, hue set to 0");
      hue = 0;
    }
    Digitdisplay();
  }
  
  //button is open
  if(digitalRead(button_pullup) == HIGH) {
      if(!wasopen) {
        wasopen = true;
      }
      if(state){
        bigpixel.clear();
        float to16bit = (hue / 360.0) * 65535.0;
        bigpixel.setPixelColor(0,bigpixel.ColorHSV(to16bit,255,255));
        bigpixel.show();
      }
      else {
        bigpixel.clear();
        bigpixel.show();
      }
  }
  //button is closed
  else {
    if(wasopen){
      state = !state;
      wasopen = false;
    }
    }
}

void Digitdisplay() {
    if(hue == 0){
      tm1637.displayStr("   0");
    }
    else {
    tm1637.displayNum(hue);
    }
    delay(50);
}
```

With this code, you can send a GET request to the boards IP adress `[ip]/led?ledstate=on` to turn the small LED on or if you replace on with anything else it turns off.

Another functionality lets you send `[ip]/neopixel?[parameter]` and control the adressable LED on the board. Notably the LED is not actually an Neopixel as it is a trademark but a similar product from another company.

The code has the following functionality:

- `hue=[float number]` lets you control the hue of the LED
- `brightness=[float number]` lets you control the brightness of the LED
- `hue=get` fetches the current hue of the LED
- `brightness=get` fetches the current brightness of the LED

Notably if you send a request with a non-number or an illegal float number, the API will assume the value to be 0 or in the case of brightness the negative brightness will result in no light at all.

Here is a video of me controlling the small LED, changing the hue and brightness of the adressable LED and reading its status using Postman:

{{< video src="wifidemo.mp4" alt="PCB with an led changing colors and brightness">}} I'm in control, baby! {{</ image>}}
