---
title: About
---

Hi! I am Hiski Huovila, a student of [Information Networks](https://informaatioverkostot.fi) with an interest in games, art and technology. I’m not an expert of a specific field (though I’m fairly good at a certain few) but instead a collection of multiple skills & fields. Generally I would describe myself as a game designer, programmer and an artist.

### What is this website

I took the Digital Fabrication minor in spring of 2023. The courses follow the global [Fab Academy](https://fabacademy.org/) schedule during which students learn [how to make almost anything](https://fabacademy.org/about/course.html#how-to-make-almost-anything). This site is both the first assignment of Fab Academy and a showcase of all of the works I made during the courses.

### My history with fabrication

I've always loved crafts. I learned woodworking in middle school and I've been working with clay weekly for over 10 years. As a hobby I've also built stuff with Raspberry Pi's and a smart mirror from an old tablet. However I was missing two major fabrication mediums: 3D printing and laser cutting. I was really excited to finally get to learn these tools and skills to make literally anything.

### More of my works

- You can find my clay and crafts on my Instagram account [@hiskimakesthings](https://www.instagram.com/hiskimakesthings/)
- You can play my games and download my game-related pdfs from [games.hiskih.fi](https://games.hiskih.fi)

### Related links

- [Aalto Fablab](https://studios.aalto.fi/fablab)
- [Aalto University](https://aalto.fi/en)
