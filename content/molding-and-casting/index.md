---
title: Molding and casting
menu: project
weight: 11
---

Is making blue cheese considered molding and casting? This week we are supposed to 3d mill a master mold, cast a silicone mold from that and then cast plastic with the silicone mold. The final object should be simple enough that it can be done quickly and without spending too much materials. I decided to make a 20-sided polyhedron, also known as an icosahedron. These are commonly used as 20-sided dice in roleplaying games.

### The master mold

#### Milling foam

After some research I ended up using FreeCAD to model my mold. I used the `Polyhedrons` macro addon to generate a 16mm icosahedron and the `Manipulation workbench` addon to align the model properly. Then I merged the model and a box together, added 10mm walls and here is the result rendered in Preform:

{{< image src="mastermoldmodel.jpg" alt="3d model of a mold">}} Nice, a slice. {{</ image>}}

I entered the model into Vcarve following the instruction video to a tee. Here are my toolpaths:

{{< image src="toolpaths.jpg" alt="Toolpaths in a computer">}} All set {{</ image>}}

Then I ran the milling in air to make sure everything was inputted correctly.

{{< image src="millingair.jpg" alt="Milling air">}} This feels weird {{</ image>}}

After which I ran t the milling on a block of foam. A rough cut with a 6mm flatend tool and cleanup using a 3mm ball nose tool.

{{< image src="millingfoam.jpg" alt="Milling a block of foam">}} Its a start {{</ image>}}

The result was promising.

{{< image src="milledfoam.jpg" alt="Half of an icosahedron milled in foam">}} It works {{</ image>}}

#### Milling wax

Now it was time to do the actual milling. I put in the same tools and settings while accounting for wax instead of foam and got milling.

{{< image src="millingwax.jpg" alt="Milling wax">}} The wax is so nice to mill {{</ image>}}

The result was otherwise fine but the surface was kind of rough. Worse than a 3d-print.

{{< image src="waxbad.jpg" alt="Half of an icosahedron milled in wax">}} Looks kinda rough {{</ image>}}

So I tried heating it up. The melted surface shows promise, but the wax also warps from heat.

{{< image src="waxmelted.jpg" alt="The wax mold melted">}} Ughh {{</ image>}}

I realized my stepover had been way too big so I changed it from 0.5 to 0.1 and milled the whole thing with the 6mm flatend tool to get nice sharp angles.

{{< image src="stepover.jpg" alt="Settings tab for the 6mm tool">}} Lets not overstep {{</ image>}}

The result was pretty good.

{{< image src="waxgood.jpg" alt="Half of an icosahedron milled in wax">}} Now we are talking {{</ image>}}

### Casting

Casting volatile materials requires a ton of safety. For this assignment I used a well ventilated room to handle the Silicon and eventually used the atex room for the smelly resin because the atex room has a ventilated wall that sucks up the fumes. For safety measures I used vinyl gloves, long sleeves and a face covering visor.

#### Silicon

Now was time to cast the mold. I used Smooth-On slow drying silicon. It has a 1:1 mixing ratio by size, pot life of 50 minutes and drying time of 4 hours.

{{< image src="silicone.jpg" alt="Materials for silicone pour">}} Getting ready {{</ image>}}

The mixed solution needed to be degassed to get the best bubble free cast, so I used a depressurized chamber to remove air.

{{< image src="depressurize.jpg" alt="Depressurization chamber">}} This sucks {{</ image>}}

After which I poured the silicon to the master mold. Notably I did not have enough silicon to fill the whole mold as I did not take into account that some of the silicon would stick to the pouring cup. About 5g of silicon to be precise.

{{< image src="pouredsilicon.jpg" alt="Freshly mixed silicone in a mold">}} Don't eat moldy bread {{</ image>}}

I left the cast to cure for the night and the next day I got a nice silicon mold.

{{< image src="finishedsilicon.jpg" alt="A silicone mold">}} Nice and smooth {{</ image>}}

Then the urethane resin. Also by Smooth-On. The resin is mized 1:1 by volume or 100:90 by weight. It has a pot life of 7 minutes and cures in half an hour. This one I mixed in the atex room for best possible ventilation.

{{< image src="mixingresin.jpg" alt="Materials for resin pour">}} Now we pour {{</ image>}}

The resin was way easier to mix than the silicon due to it being a lot more fluid. I quickly mixed the parts together and poured the resin into the mold. This time I mixed plenty of resin to not run out like I did with the silicon.

{{< image src="pouredresin.jpg" alt="Freshly mixed resin in a mold">}} Cook for 30 minutes in room temperature {{</ image>}}

30 minutes later the cured resin popped out of the mold nicely.

{{< image src="curedresin.jpg" alt="Half of an icosahedron cast from white plastic">}} Now for the second half {{</ image>}}

I mixed another cup of resin and cast another half to my die.

{{< image src="parts.jpg" alt="Two halves of an icosahedron, a silicone mold and a wax mold">}} Some assembly required {{</ image>}}

Finally I sanded the uneven top off from both parts tha was formed due to surface tension. I measured the weight of bot halves to 0.1g precision so that the die remains somewhat balanced. Finally, I glued the parts together using Zap-a-gap. Some final sanding to smooth out the outside and we are finished:

{{< image src="joined.jpg" alt="A white 20 sided die without numbers">}} It's done! {{</ image>}}
